export const Information = {
  name: "ÖKÜZEFENDİ BURGER HOUSE",
  shortname: "Öküzefendi Burger",
  logo: require("./img/transprant.png"),
  drawerBackground: require("./img/bg.jpeg"),
  imageEndPoint:
    "https://cdn.yemeksepeti.com//ProductImages/TR_DENIZLI/okuzefendi_burger_house",
  facebook: "fb://page/771861239688534/", // find on https://findmyfbid.com/
  twitter: "https://www.twitter.com/okancancosar",
  instagram: "https://www.instagram.com/okancancosar/",
  youtube: "https://www.youtube.com/channel/UCv6jcPwFujuTIwFQ11jt1Yw"
};

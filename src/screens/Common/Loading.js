import React, { Component } from "react";
import { StyleSheet, View, Text, ActivityIndicator } from "react-native";

class Loading extends Component {
  render() {
    const { text, icon } = this.props;
    return (
      <View style={styles.center}>
        {icon ? <ActivityIndicator size="large" color="#0f0" /> : <View />}
        <Text>{text}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export { Loading };

import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

/*
  *   Tek çizgi içine metin olarak tasarlanmıştır. Ayıraç görevi görür.
  * props:
      text => iki çizgi arasına gelecek yazı
*/
class Separator extends Component {
  render() {
    const { separatorLine, separatorText, separatorContainer } = styles;
    return (
      <View style={separatorContainer}>
        <View style={separatorLine} />
        <Text style={styles.separatorText}>{this.props.text}</Text>
        <View style={separatorLine} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  separatorContainer: {
    alignItems: "center",
    flexDirection: "row"
  },
  separatorLine: {
    flex: 1,
    borderWidth: StyleSheet.hairlineWidth,
    height: StyleSheet.hairlineWidth,
    borderColor: "#9B9FA4"
  },
  separatorText: {
    color: "#9B9FA4"
  }
});

export { Separator };

import React, { Component } from "react";
import { TouchableWithoutFeedback, Keyboard } from "react-native";

/*
  * input girişi yapıldıktan sonra boşluğa basıldığında Keyboard'ı kapatmak için yazılmıştır.
  * props:

*/
class TouchDismiss extends Component {
  render() {
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {this.props.children}
      </TouchableWithoutFeedback>
    );
  }
}

export { TouchDismiss };

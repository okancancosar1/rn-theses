import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  ToastAndroid,
  TouchableNativeFeedback
} from "react-native";
import { Icon } from "react-native-elements";
import IconBadge from "react-native-icon-badge";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Information } from "../../../Ortak";
import { getChartLenght } from "../../actions/Header";

class Header extends Component {
  constructor(props) {
    super(props);
    if (this.props.isAuth) this.props.actions.getChartLenght(this.props.isWork);
  }

  render() {
    const { isOpen, nav } = this.props;
    let bgc = "#ff0000";
    let clr = "#fff";

    if (isOpen) {
      bgc = "#00FF00";
      clr = "#000";
    }

    return (
      <View style={[styles.container, { backgroundColor: bgc }]}>
        <View style={styles.MH10}>
          <TouchableNativeFeedback
            style={styles.MH10}
            onPress={() => {
              console.log("::HEADER:: try to open Drawer");
              nav.navigate("DrawerToggle");
            }}
          >
            <Icon name="menu" size={32} color={clr} />
          </TouchableNativeFeedback>
        </View>

        <TouchableNativeFeedback
          onPress={() => {
            console.log("::smaine gidiyoz::");
            if (nav.state.routeName != "S_Main") nav.navigate("S_Main");
          }}
        >
          <Image style={styles.logo} source={Information.logo} />
        </TouchableNativeFeedback>

        <TouchableNativeFeedback
          onPress={() => {
            console.log("::HEADER:: try to open S_Cart");
            if (nav.state.routeName != "S_Cart") nav.navigate("S_Cart");
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              marginRight: 10
            }}
          >
            <IconBadge
              MainElement={<Icon name="shopping-cart" size={32} color={clr} />}
              BadgeElement={
                <Text style={styles.badgeText}>
                  {this.props.sepetBuyuklugu}
                </Text>
              }
              IconBadgeStyle={styles.iconbadge}
              Hidden={this.props.sepetBuyuklugu == 0}
            />
          </View>
        </TouchableNativeFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "relative",
    top: 0,
    height: 50,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  logo: {
    height: 40,
    width: 40
  },
  MH10: {
    marginHorizontal: 10
  },
  badgeText: {
    color: "#f00",
    fontSize: 13
  },
  iconbadge: {
    width: 5,
    height: 15,
    backgroundColor: "#ff0"
  }
});

export default connect(
  state => ({
    isOpen: state.R_Welcome.isOpen,
    isAuth: state.R_Welcome.isAuth,
    sepetBuyuklugu: state.Header.sepetBuyuklugu,
    isWork: state.Header.isWork
  }),
  dispatch => ({
    actions: bindActionCreators(Object.assign({ getChartLenght }), dispatch)
  })
)(Header);

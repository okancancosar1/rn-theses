import React, { Component } from "react";
import { ActivityIndicator, TextInput, View, ToastAndroid } from "react-native";
import firebase from "firebase";
import { Card, Button, FormValidationMessage } from "react-native-elements";

import Header from "../Common/Header";

class S_ResetPass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mail: "",
      loading: false,
      error: ""
    };
  }

  _resetPasswordd = async () => {
    this.setState({ loading: true });
    return await firebase
      .auth()
      .sendPasswordResetEmail(this.state.mail)
      .then(() => {
        this.setState({ loading: false, error: "" });
        ToastAndroid.show(
          "Mailinizi sıfırlama isteği eposta adresinize yollandı\nBirkaç dakika içinde elinize ulaşır.",
          ToastAndroid.LONG
        );
      })
      .catch(error => {
        this.setState({ loading: false, error: error });
        console.log("::RESET PASSWORD:: BIŞEY OLDU.");
      });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header nav={this.props.navigation} />
        <Card title="Şifrenizi mi unuttunuz?">
          <TextInput
            value={this.props.email}
            placeholder={"Kayıtlı mail adresiniz."}
            multiline={false}
            autoCorrect={false}
            autoCapitalize={"none"}
            keyboardType={"email-address"}
            returnKeyType={"done"}
            onSubmitEditing={() => this._resetPasswordd()}
            onChangeText={mail => {
              this.setState({ mail });
            }}
          />
          {this.state.loading ? (
            <ActivityIndicator size="large" color="#0f0" />
          ) : (
            <Button
              style={{ marginVertical: 20 }}
              title="Sıfırlama maili gönder"
              onPress={() => this._resetPasswordd()}
            />
          )}

          {this.props.error ? (
            <FormValidationMessage>{this.props.error}</FormValidationMessage>
          ) : (
            <View />
          )}
        </Card>
      </View>
    );
  }
}

export default S_ResetPass;

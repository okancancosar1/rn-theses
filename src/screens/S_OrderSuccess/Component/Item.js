import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableWithoutFeedback
} from "react-native";

import { Icon } from "react-native-elements";
import { Separator } from "../../Common";
import { firebaselestirme } from "../../../configs/initFirebase";

class Item extends Component {
  render() {
    const { item } = this.props;

    let tumEkMetin = "Ek: ";

    if (item.urun.ekler) {
      tumEkler = firebaselestirme(item.urun.ekler);
      let defaultExtralar = [];
      let defaultEkler = "";
      tumEkler.forEach(element => {
        if (element.baslik == "Ekstra Malzeme") defaultExtralar = element;
        else if (element.baslik == "İçecek Seçimi") defaultEkler = element;
      });

      if (item.ekUrunler) {
        if (item.ekUrunler.drink) {
          const seciliIcecekKey = item.ekUrunler.drink[0];
          // const defaultEkler = tumEkler[1];

          if (defaultEkler.secenekler) {
            const defff = firebaselestirme(defaultEkler.secenekler);
            defff.forEach(element => {
              if (element.key == seciliIcecekKey) {
                return (tumEkMetin += element.ad + " ");
              }
            });
          }
        }
        if (item.ekUrunler.extra) {
          const seciliExtraKeyleri = item.ekUrunler.extra;
          // const defaultExtralar = tumEkler[0];

          seciliExtraKeyleri.forEach(seciliKey => {
            if (defaultExtralar.secenekler) {
              const sds = firebaselestirme(defaultExtralar.secenekler);

              sds.forEach(item => {
                if (item.key == seciliKey) {
                  tumEkMetin += item.ad + " ";
                }
              });
            }
          });
        }
      }
    }

    return (
      <TouchableWithoutFeedback>
        <View style={styles.container}>
          <View style={styles.row}>
            <View style={styles.addCont}>
              <Image
                style={styles.imageStyle}
                source={{
                  uri: `https://dummyimage.com/100/fff2f2/000.png&text=${
                    item.urunAdedi
                  }x`
                }}
              />
            </View>
            <View style={styles.infoCont}>
              <Text style={styles.productName}> {item.urun.ad} </Text>
              <Text style={styles.productExp} numberOfLines={2}>
                {tumEkMetin != "Ek: " ? tumEkMetin : item.urun.aciklama}
              </Text>
            </View>
            <View style={styles.priceCont}>
              <Icon
                type={"font-awesome"}
                iconStyle={styles.tryIconStyle}
                name={"try"}
                size={15}
                color={"gray"}
              />
              <Text style={styles.price}>{item.fiyat.toFixed(2)}</Text>
            </View>
          </View>
          <Separator />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginRight: 10,
    flex: 1
  },
  row: {
    flex: 1,
    flexDirection: "row"
  },
  addCont: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  infoCont: {
    flex: 5,
    marginLeft: 5
  },
  priceCont: {
    flex: 1,
    flexDirection: "row"
  },
  productName: {
    color: "red",
    opacity: 0.9,
    fontSize: 13
  },
  productExp: {
    fontSize: 12,
    marginVertical: 5,
    opacity: 0.5
  },
  price: {
    flex: 1,
    opacity: 0.5,
    color: "red",
    fontSize: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  addictionalIconStyle: {
    opacity: 0.5,
    color: "red",
    marginRight: 5,
    borderRightColor: "red",
    borderRightWidth: StyleSheet.hairlineWidth
  },
  tryIconStyle: {
    flex: 1,
    opacity: 0.5,
    color: "red",
    alignItems: "center",
    justifyContent: "center"
  },
  imageStyle: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginRight: 5
  }
});

export { Item };

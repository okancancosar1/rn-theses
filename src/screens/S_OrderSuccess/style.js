import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  head: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30
  },
  ikon: {
    marginTop: 20
  },
  headtext: {
    fontFamily: "Iowan Old Style",
    color: "red",
    opacity: 0.6,
    fontSize: 23
  },
  siparisBilgileri: {
    margin: 7,
    borderRadius: 4,
    borderWidth: 0.1,
    backgroundColor: "#eff4f7"
  },
  gonderimBilgileri: {
    margin: 7,
    marginTop: -3,
    borderRadius: 4,
    borderWidth: 0.1,
    backgroundColor: "#eff4f7"
  },
  urunler: {
    margin: 7,
    marginTop: -3,
    paddingBottom: 40,
    borderRadius: 4,
    borderWidth: 0.1,
    backgroundColor: "#eff4f7"
  },
  atview: {
    alignItems: "flex-start",
    justifyContent: "center"
  },
  altBaslik: {
    margin: 10,
    fontSize: 17
  },
  id: {
    color: "blue",
    fontSize: 16,
    marginLeft: 10,
    marginVertical: 7
  },
  row: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginHorizontal: 15,
    marginVertical: 5
  },
  rowx: {
    flex: 1,
    opacity: 0.7
  },
  rowy: {
    flex: 1,
    opacity: 0.7,
    marginBottom: 5
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    height: 50,
    backgroundColor: "#51FFBC",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-around"
  },
  footertext: {
    fontSize: 20
  }
});

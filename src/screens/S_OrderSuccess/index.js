import React, { Component } from "react";
import {
  Text,
  View,
  ScrollView,
  FlatList,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Icon } from "react-native-elements";
import moment from "moment";
import firebase from "firebase";

import { getOrderDetail } from "../../actions/A_OrderSuccess";
import { Separator } from "../Common";
import Header from "../Common/Header";
import { Item } from "./Component";
import { firebaselestirme } from "../../configs/initFirebase";

import { styles } from "./style";
import tr from "moment/locale/tr";
moment.updateLocale("tr", tr);

class S_OrderSuccess extends Component {
  componentDidMount() {
    const { params } = this.props.navigation.state;
    this.props.actions.getOrderDetail(params.newKey);
  }

  render() {
    const { params } = this.props.navigation.state;
    const {
      durum,
      odemeYontemi,
      adres,
      tarih,
      tutar,
      siparisNotu
    } = this.props.data;

    let telefon = "";
    let tumAdres = "";
    if (adres) {
      telefon = adres.telefon;
      tumAdres = adres.tumAdres;
    }

    if (this.props.error) {
      return (
        <View style={styles.container}>
          <Header nav={this.props.navigation} />
          <View style={styles.head}>
            <Text style={styles.headtext}>{this.props.error}</Text>
          </View>
        </View>
      );
    }
    if (this.props.loading) {
      return (
        <View style={styles.container}>
          <Header nav={this.props.navigation} />
          <View style={styles.head}>
            <ActivityIndicator size="large" color="#0f0" />
            <Text style={styles.headtext}>{this.props.error}</Text>
          </View>
        </View>
      );
    }
    return (
      <ScrollView style={styles.container}>
        <Header nav={this.props.navigation} />

        <View style={styles.head}>
          <Text style={styles.headtext}> Siparişiniz alındı.</Text>
          <Icon style={styles.ikon} name="done" size={40} color={"red"} />
        </View>
        <View style={styles.siparisBilgileri}>
          <View style={styles.atview}>
            <Text style={styles.altBaslik}> Sipariş Detayları </Text>
            <Separator />
            <Text style={styles.id}> #{params.newKey.slice(-7)} </Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.rowx}> Tarih </Text>
            <Text style={styles.rowy}>
              {moment(new Date(tarih).toLocaleString()).format(
                "DD.MM.YYYY HH:mm"
              )}
            </Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.rowx}> Durum </Text>
            <Text style={styles.rowy}>{durum}</Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.rowx}> Ödeme Yöntemi </Text>
            <Text style={styles.rowy}> {odemeYontemi} </Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.rowx}> Sipariş Toplamı </Text>
            <Text style={styles.rowy}> {tutar} TL</Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.rowx}> Sipariş Notu </Text>
            <Text style={styles.rowy}>{siparisNotu}</Text>
          </View>
        </View>

        <View style={styles.gonderimBilgileri}>
          <View style={styles.atview}>
            <Text style={styles.altBaslik}> Gönderim Detayları </Text>
            <Separator />
          </View>

          <View style={styles.row}>
            <Text style={styles.rowx}> Kişi </Text>
            <Text style={styles.rowy}>
              {firebase.auth().currentUser.displayName}
            </Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.rowx}> İrtibat Tel </Text>
            <Text style={styles.rowy}>{telefon}</Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.rowx}> Adres </Text>
            <Text style={styles.rowy} numberOfLines={5}>
              {tumAdres}
            </Text>
          </View>
        </View>
        <View style={styles.gonderimBilgileri}>
          <View style={styles.atview}>
            <Text style={styles.altBaslik}> Ürünler </Text>
            <Separator />
          </View>
          <FlatList
            style={{ margin: 5 }}
            data={this.props.data.urunler}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => <Item item={item} />}
          />
        </View>
      </ScrollView>
    );
  }
}

export default connect(
  state => ({
    data: state.R_OrderSuccess.data,
    loading: state.R_OrderSuccess.loading,
    error: state.R_OrderSuccess.error
  }),
  dispatch => ({
    actions: bindActionCreators(Object.assign({ getOrderDetail }), dispatch)
  })
)(S_OrderSuccess);

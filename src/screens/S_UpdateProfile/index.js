import React, { Component } from "react";
import { TextInput, Text, View, Image, ActivityIndicator } from "react-native";
import { Button, FormValidationMessage } from "react-native-elements";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import {
  nameChanged,
  surnameChanged,
  phoneChanged,
  updateUser
} from "../../actions/A_UpdateProfile";

import { styles } from "./style";
import { Information } from "../../../Ortak";
import { TouchDismiss } from "../Common";
import Header from "../Common/Header";

class S_UpdateProfile extends Component {
  _pressHandle = () => {
    const { name, surname, phone, navigation } = this.props;
    this.props.actions.updateUser(name, surname, phone, navigation);
  };
  createButton = () => {
    if (this.props.loading) return <ActivityIndicator size="large" />;
    else
      return (
        <Button
          style={styles.button}
          title="Güncelle"
          color="#49505f"
          onPress={this._pressHandle}
        />
      );
  };
  selectPic = () => {
    // console.log("::::", );
  };

  render() {
    return (
      <TouchDismiss>
        <View style={styles.container}>
          <Header nav={this.props.navigation} />
          <View style={styles.formContainer}>
            <Image
              style={styles.logo}
              source={Information.logo}
              onPress={this.selectPic}
            />

            <View style={styles.inputCont}>
              <TextInput
                ref={a => (this.input1 = a)}
                value={this.props.name}
                placeholder={"İsim"}
                style={styles.inpt}
                multiline={false}
                autoCorrect={false}
                returnKeyType={"next"}
                onSubmitEditing={() => this.input2.focus()}
                onChangeText={s => {
                  this.props.actions.nameChanged(s);
                }}
              />
              <TextInput
                ref={a => (this.input2 = a)}
                value={this.props.surname}
                placeholder={"Soyisim"}
                style={styles.inpt}
                multiline={false}
                autoCorrect={false}
                returnKeyType={"next"}
                onSubmitEditing={() => this.input3.focus()}
                onChangeText={s => {
                  this.props.actions.surnameChanged(s);
                }}
              />

              <TextInput
                ref={a => (this.input3 = a)}
                value={this.props.phone}
                placeholder={"Telefon"}
                style={styles.inpt}
                multiline={false}
                autoCorrect={false}
                keyboardType={"numeric"}
                returnKeyType={"done"}
                onChangeText={s => {
                  this.props.actions.phoneChanged(s);
                }}
              />

              <View style={styles.buttonCont}>{this.createButton()}</View>
              <FormValidationMessage>
                {this.props.emailError}
              </FormValidationMessage>
            </View>
          </View>
        </View>
      </TouchDismiss>
    );
  }
}

export default connect(
  state => ({
    name: state.R_UpdateProfile.name,
    surname: state.R_UpdateProfile.surname,
    phone: state.R_UpdateProfile.phone,
    loading: state.R_UpdateProfile.loading,
    error: state.R_UpdateProfile.error
  }),
  dispatch => ({
    actions: bindActionCreators(
      Object.assign(
        { nameChanged },
        { surnameChanged },
        { phoneChanged },
        { updateUser }
      ),
      dispatch
    )
  })
)(S_UpdateProfile);

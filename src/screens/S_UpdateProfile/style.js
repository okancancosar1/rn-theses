import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  logoContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  formContainer: {
    flex: 4,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  },
  lastContainer: {
    flex: 1
  },
  logo: {
    height: 100,
    width: 100
  },
  inputCont: {
    flex: 4,
    marginVertical: 20,
    marginHorizontal: width / 20
  },
  inpt: {
    marginBottom: 10
  },
  buttonCont: {
    flex: 1,
    marginHorizontal: width / 5,
    marginTop: 30
  },

  girisBtnCont: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 7
  },
  girisBtn: {
    flex: 1,
    fontWeight: "bold",
    fontSize: 16,
    opacity: 0.5,
    marginTop: 15
  }
});

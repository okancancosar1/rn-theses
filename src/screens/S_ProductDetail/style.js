import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  headCont: {
    margin: 5
  },
  infoCont: {
    flexDirection: "row",
    margin: 5
  },
  infoImage: {
    height: 100,
    width: 100
  },
  infoTextCont: {
    flex: 1,
    alignItems: "flex-start",
    marginHorizontal: 5
  },
  name: {
    fontSize: 15,
    marginBottom: 5,
    letterSpacing: 0.5,
    fontWeight: "400",
    color: "red"
  },
  exp: {
    fontSize: 12,
    marginBottom: 10
  },

  counterCont: {
    flexDirection: "row",
    justifyContent: "space-between",
    margin: 5
  },
  counterC: {
    flex: 3,
    flexDirection: "row",
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: "black"
  },
  iconStyle: {
    opacity: 0.5
  },
  priceCont: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
    flexDirection: "row"
  },
  favCont: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5
  },
  counter: {
    color: "darkred",
    fontSize: 20
  },
  price: {
    marginTop: -2,
    marginLeft: 5,
    color: "#000",
    fontWeight: "bold"
  },
  propsCont: {
    flex: 3,
    margin: 5
  },
  artiCont: {
    flex: 2
  },
  sayactext: {
    flex: 3,
    alignItems: "center",
    justifyContent: "center"
  },
  emptycont: {
    flex: 1
  },
  pickerCont: {
    flex: 1,
    flexDirection: "row",
    marginTop: 10
  },
  picker: {
    flex: 1
  },
  pickerText: {
    marginTop: 17
  },
  MT10: {
    marginTop: 10
  },
  selectsty: {
    backgroundColor: "#dfefef",
    paddingVertical: 5
  },
  buttonsCont: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableNativeFeedback,
  ScrollView,
  ToastAndroid,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Icon, Button, FormValidationMessage } from "react-native-elements";
import SectionedMultiSelect from "react-native-sectioned-multi-select";

import {
  getProductDetail,
  productCountIncrease,
  productCountDecrease,
  changeDrinkSelectedItems,
  changeExtraSelectedItems,
  updatePrice,
  addToCart,
  resetReducer,
  checkIsFav,
  addToFav,
  rmToFav
} from "../../actions/A_ProductDetail";
import { firebaselestirme } from "../../configs/initFirebase";
import { Separator } from "../Common";
import { styles } from "./style";
import { Information } from "../../../Ortak";
import Header from "../Common/Header";

class S_ProductDetail extends Component {
  componentDidMount() {
    const { params } = this.props.navigation.state.params;
    this.props.actions.getProductDetail(params);
    this.props.actions.checkIsFav(params);
  }
  // componentWillUnmount() {
  //   /**
  //    * sayfa değiştiğinde storedaki elemanları silmek lazım yoksa tekrar geliyor.
  //    */
  //   this.props.actions.resetReducer();
  // }

  createButton = () => {
    if (this.props.loading) return <ActivityIndicator size="large" />;
    else
      return (
        <View style={styles.buttonsCont}>
          <Button
            title="Sepete Ekle"
            onPress={() => {
              if (this.props.isAuth) {
                this.props.actions.addToCart(
                  {
                    urunAdedi: this.props.productCount,
                    urun: this.props.productDetail,
                    fiyat: this.props.totalPrice,
                    ekUrunler: {
                      drink: this.props.drinkSelectedItems,
                      extra: this.props.extraSelectedItems
                    }
                  },
                  this.props.navigation
                );
              } else {
                ToastAndroid.show(
                  "Önce giriş yapmalısınız.",
                  ToastAndroid.SHORT
                );
              }
            }}
          />
          <FormValidationMessage> {this.props.error} </FormValidationMessage>
        </View>
      );
  };

  calcPrice = selectedItemObjects => {
    let val = 0;
    selectedItemObjects.forEach((value, i) => {
      val += Number(value.more.fiyat);
    });
    return val;
  };

  renderSingleSelect = singleSelectArray => {
    if (singleSelectArray.length != 0) {
      return (
        <View style={styles.MT10}>
          <Text> İçecek Seçimi: </Text>
          <View style={styles.selectsty}>
            <SectionedMultiSelect
              selectText="Seçin"
              confirmText="Ekle"
              selectedText="seçildi"
              searchPlaceholderText="Ara"
              items={singleSelectArray}
              style={styles.selectsty}
              single={true}
              uniqueKey="id"
              subKey="children"
              showDropDowns={false}
              readOnlyHeadings={true}
              selectedItems={this.props.drinkSelectedItems}
              onSelectedItemsChange={item => {
                this.props.actions.changeDrinkSelectedItems(item);
              }}
              onSelectedItemObjectsChange={selectedItemObjects => {
                // seçilen içeceğe göre fiyat güncellendi
                this.props.actions.updatePrice(
                  "drinkPrice",
                  Number(selectedItemObjects[0].more.fiyat)
                );
              }}
            />
          </View>
        </View>
      );
    }
  };

  renderMultipleSelect = multiSelectArray => {
    if (multiSelectArray.length != 0) {
      return (
        <View style={styles.MT10}>
          <Text> Extra seçimi: </Text>
          <View style={styles.selectsty}>
            <SectionedMultiSelect
              selectText="Seçin"
              confirmText="Ekle"
              selectedText="seçildi"
              searchPlaceholderText="Ara"
              items={multiSelectArray}
              uniqueKey="id"
              subKey="children"
              showDropDowns={false}
              readOnlyHeadings={true}
              selectedItems={this.props.extraSelectedItems}
              onSelectedItemsChange={item => {
                this.props.actions.changeExtraSelectedItems(item);
              }}
              onSelectedItemObjectsChange={selectedItemObjects => {
                // seçilen extraya göre fiyat güncellendi
                this.props.actions.updatePrice(
                  "extraPrice",
                  Number(this.calcPrice(selectedItemObjects))
                );
              }}
            />
          </View>
        </View>
      );
    }
  };

  render() {
    const img = this.props.productDetail.img
      ? `${Information.imageEndPoint}${this.props.productDetail.img}`
      : null;

    // ürün fiyatını güncellendi
    this.props.actions.updatePrice(
      "productPrice",
      Number(this.props.productDetail.fiyat)
    );

    // ürün sayısına göre fiyat güncellendi
    this.props.actions.updatePrice(
      "total",
      Number(this.props.productCount) *
        (Number(this.props.productPrice) +
          Number(this.props.drinkPrice) +
          Number(this.props.extrasPrice))
    );

    /**
     * ek ürünlerin ayarlanması
     */
    let singleSelectArray = [];
    let multiSelectArray = [];
    const ekParams = this.props.productDetail.ekler
      ? firebaselestirme(this.props.productDetail.ekler)
      : [];
    if (ekParams.length != 0) {
      for (let i = 0; i < ekParams.length; i++) {
        // 1 tane ek ürün seçilebilecek liste
        if (ekParams[i].tip == 1) {
          const tip1Secenekler = ekParams[i].secenekler
            ? firebaselestirme(ekParams[i].secenekler)
            : [];

          let all = {};
          all.name = ekParams[i].baslik;
          all.id = ekParams[i].id;
          all.children = [];

          tip1Secenekler.forEach((value, i) => {
            let ekUcret = value.fiyat != 0 ? `[ ${value.fiyat} TL ekle ]` : "";
            let di = {
              id: value.id,
              ad: value.ad,
              key: value.key,
              fiyat: value.fiyat
            };
            let ary = {
              id: value.key,
              name: `${value.ad} ${ekUcret}`,
              more: di
            };
            all.children.push(ary);
          });
          singleSelectArray.push(all);
        }
        //  birden fazla seçim yapılabilecek liste
        else if (ekParams[i].tip == 2) {
          const tip2Secenekler = ekParams[i].secenekler
            ? firebaselestirme(ekParams[i].secenekler)
            : [];

          let all = {};
          all.name = ekParams[i].baslik;
          all.id = ekParams[i].id;
          all.children = [];

          tip2Secenekler.forEach((value, i) => {
            let ekUcret = value.fiyat != 0 ? `[ ${value.fiyat} TL ekle ]` : "";
            let di = {
              id: value.id,
              ad: value.ad,
              key: value.key,
              fiyat: value.fiyat
            };
            let ary = {
              id: value.key,
              name: `${value.ad} ${ekUcret}`,
              more: di
            };
            all.children.push(ary);
          });
          multiSelectArray.push(all);
        }
      }
    } else console.log("ek ürün yok");

    if (this.props.detailError) {
      return (
        <View style={styles.container}>
          <Header nav={this.props.navigation} />
          <Text>Böyle bir ürün bulunamadı hata: {this.props.detailError}</Text>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <Header nav={this.props.navigation} />

        {/* page Name */}
        <View style={styles.headCont}>
          <Text> Ürün Detayı </Text>
          <Separator />
        </View>

        {/* Ürün bilgileri */}
        <View style={styles.infoCont}>
          <Image style={styles.infoImage} source={{ uri: img }} />
          <View style={styles.infoTextCont}>
            <Text style={styles.name}>{this.props.productDetail.ad}</Text>
            <Text style={styles.exp}>{this.props.productDetail.aciklama}</Text>
          </View>
        </View>
        <Separator />

        <ScrollView>
          {/* Sayaç bölümü */}
          <View style={styles.counterCont}>
            <View style={styles.counterC}>
              <View style={styles.emptycont} />
              <TouchableNativeFeedback
                style={styles.artiCont}
                onPress={() => {
                  const pc = this.props.productCount;
                  if (pc < 10) {
                    this.props.actions.productCountIncrease(pc);
                  }
                }}
              >
                <Icon
                  type="font-awesome"
                  iconStyle={styles.iconStyle}
                  name="plus"
                  size={28}
                />
              </TouchableNativeFeedback>
              <View style={styles.sayactext}>
                <Text style={styles.counter}>
                  {this.props.productCount} Adet
                </Text>
              </View>

              <TouchableNativeFeedback
                style={styles.eksicont}
                onPress={() => {
                  const pc = this.props.productCount;
                  if (pc > 1) {
                    this.props.actions.productCountDecrease(pc);
                  }
                }}
              >
                <Icon
                  type="font-awesome"
                  iconStyle={styles.iconStyle}
                  name="minus"
                  size={25}
                />
              </TouchableNativeFeedback>
              <View style={styles.emptycont} />
            </View>
            <View style={styles.priceCont}>
              <Icon type="font-awesome" name="try" size={15} />
              <Text style={styles.price}>
                {this.props.totalPrice.toFixed(2)}
              </Text>
            </View>
            <View style={styles.favCont}>
              {this.props.isFav != "" ? (
                <Icon
                  iconStyle={styles.iconStyle}
                  name="favorite"
                  size={25}
                  onPress={() => {
                    if (this.props.isAuth)
                      this.props.actions.rmToFav(this.props.isFav);
                    else
                      ToastAndroid.show(
                        "Önce giriş yapın.",
                        ToastAndroid.SHORT
                      );
                  }}
                />
              ) : (
                <Icon
                  iconStyle={styles.iconStyle}
                  name="favorite-border"
                  size={25}
                  onPress={() => {
                    if (this.props.isAuth)
                      this.props.actions.addToFav(
                        this.props.productDetail.key,
                        this.props.productDetail.ad,
                        this.props.productDetail.aciklama,
                        this.props.productDetail.fiyat
                      );
                    else
                      ToastAndroid.show(
                        "Önce giriş yapın.",
                        ToastAndroid.SHORT
                      );
                  }}
                />
              )}
            </View>
          </View>

          {/* ek seçimler */}
          <View style={styles.propsCont}>
            <View>
              <Text> Ek Seçimler </Text>
              <Separator />
            </View>
            <View style={{ flex: 1 }}>
              {this.renderSingleSelect(singleSelectArray)}
              {this.renderMultipleSelect(multiSelectArray)}
            </View>
          </View>
          {/* sepete ekle */}
          {this.createButton()}
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    isAuth: state.R_Welcome.isAuth,

    productDetail: state.R_ProductDetail.productDetail,
    detailLoading: state.R_ProductDetail.detailLoading,
    detailError: state.R_ProductDetail.detailError,

    productCount: state.R_ProductDetail.productCount,

    extraSelectedItems: state.R_ProductDetail.extraSelectedItems,
    drinkSelectedItems: state.R_ProductDetail.drinkSelectedItems,

    productPrice: state.R_ProductDetail.productPrice,
    drinkPrice: state.R_ProductDetail.drinkPrice,
    extrasPrice: state.R_ProductDetail.extrasPrice,
    totalPrice: state.R_ProductDetail.totalPrice,

    isFav: state.R_ProductDetail.isFav
  }),
  dispatch => ({
    actions: bindActionCreators(
      Object.assign(
        { getProductDetail },
        { productCountIncrease },
        { productCountDecrease },
        { changeExtraSelectedItems },
        { changeDrinkSelectedItems },
        { updatePrice },
        { addToCart },
        { resetReducer },
        { checkIsFav },
        { addToFav },
        { rmToFav }
      ),
      dispatch
    )
  })
)(S_ProductDetail);

import React, { Component } from "react";
import { Text, View, StyleSheet, TouchableNativeFeedback } from "react-native";
import { Icon, Card, Button } from "react-native-elements";
import moment from "moment";

import { Separator } from "../../Common";

import tr from "moment/locale/tr";
moment.updateLocale("tr", tr);

class Orderitem extends Component {
  render() {
    const { durum, tutar, key, tarih, siparisNotu } = this.props.item;

    let bgColor = "#fff";

    if (durum == "Yolda") bgColor = "#ccc";
    else if (durum == "İptal") bgColor = "#F00";
    else if (durum == "Tamamlandı") bgColor = "#0f0";
    return (
      <TouchableNativeFeedback
        onPress={() => {
          console.log("::key::", key);
          this.props.nav("S_OrderSuccess", { newKey: key });
        }}
      >
        <Card
          containerStyle={styles.contS}
          title={durum}
          titleStyle={{
            borderBottomColor: bgColor,
            borderBottomWidth: 5
          }}
          wrapperStyle={{}}
        >
          <View style={styles.cont}>
            <View style={styles.row}>
              <Text style={styles.rowx}> #{key.slice(-7)} </Text>
            </View>

            <View style={styles.row}>
              <Text style={styles.rowx}> Tarih </Text>
              <Text style={styles.rowy}>
                {moment(new Date(tarih).toLocaleString()).format(
                  "DD.MM.YYYY HH:mm"
                )}
              </Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.rowx}> Fiyat </Text>
              <Text style={styles.rowy}>{tutar}</Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.rowx}> Not: </Text>
              <Text style={styles.rowy}>{siparisNotu}</Text>
            </View>
          </View>
        </Card>
      </TouchableNativeFeedback>
    );
  }
}
const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginHorizontal: 10,
    marginVertical: 5
  },
  rowx: {
    flex: 1
  },
  rowy: {
    flex: 1,
    opacity: 0.7
  },
  contS: {
    padding: 5,
    margin: 5
  },
  cont: {
    flex: 1,
    marginTop: -10
  }
});

export { Orderitem };

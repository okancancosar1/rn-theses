import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { getAllOrder } from "../../actions/A_Orders";
import { styles } from "./style";
import Header from "../Common/Header";
import { Separator, Loading } from "../Common";
import { Orderitem } from "./Components";

class S_Orders extends Component {
  componentDidMount() {
    this.props.actions.getAllOrder();
  }
  render() {
    let ordrs = "";
    if (!this.props.isAuth) {
      ordrs = <Loading text="Önceki siparişlerinizi burada göreceksiniz..." />;
    } else {
      if (this.props.loading) {
        ordrs = <Loading icon text="Önceki siparişlerinizi Getiriyoruz." />;
      } else if (this.props.error) {
        ordrs = <Loading text={this.props.error} />;
      } else {
        ordrs = (
          <FlatList
            data={this.props.siparisler}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <Orderitem nav={this.props.navigation.navigate} item={item} />
            )}
          />
        );
      }
    }

    return (
      <View style={styles.container}>
        <Header nav={this.props.navigation} />
        <View style={styles.prevOrder}>
          <Text style={{ margin: 5 }}> Önceki Siparişleriniz </Text>
          <Separator />
        </View>
        {ordrs}
      </View>
    );
  }
}

export default connect(
  state => ({
    isAuth: state.R_Welcome.isAuth,
    siparisler: state.R_Orders.siparisler,
    loading: state.R_Orders.loading,
    error: state.R_Orders.error
  }),
  dispatch => ({
    actions: bindActionCreators(Object.assign({ getAllOrder }), dispatch)
  })
)(S_Orders);

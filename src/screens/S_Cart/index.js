import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import { Icon, Button } from "react-native-elements";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { getCartItems, updatePrice, deleteItem } from "../../actions/A_Cart";
import { Item } from "./Component";
import { firebaselestirme } from "../../configs/initFirebase";
import { styles } from "./style";
import { Separator, Loading } from "../Common";
import Header from "../Common/Header";

class S_Cart extends Component {
  componentDidMount() {
    if (this.props.isAuth) this.props.actions.getCartItems();
  }

  render() {
    let cart = "";
    let dsb = true;
    if (!this.props.isAuth) {
      cart = <Loading text="Sepetinizi burada göreceksiniz..." />;
    } else {
      if (this.props.loading) {
        cart = <Loading icon text="Sepetinizi Getiriyoruz." />;
      } else if (this.props.error) {
        cart = <Loading text={this.props.error} />;
      } else {
        dsb = false;
        cart = (
          <FlatList
            data={this.props.cartItems}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <Item
                item={item}
                sil={() => this.props.actions.deleteItem(item.key)}
              />
            )}
          />
        );
      }
    }

    return (
      <View style={styles.container}>
        <Header nav={this.props.navigation} />
        <View style={styles.altCont}>
          <Text style={{ margin: 5 }}> Sepetinizdekiler </Text>
          <Separator />
          {cart}
        </View>

        <View style={styles.footer}>
          <View style={styles.fiyatView}>
            <Text> Toplam </Text>
            <View style={styles.fiyat}>
              <Icon type="font-awesome" name="try" size={15} />
              <Text style={styles.price}>
                {this.props.totalPrice.toFixed(2)}
              </Text>
            </View>
          </View>
          <View style={styles.butonView}>
            <Button
              disabled={dsb}
              title="Sepeti onayla"
              buttonStyle={styles.btn}
              onPress={() => {
                this.props.navigation.navigate("S_OrderDetail", {
                  ucret: this.props.totalPrice.toFixed(2),
                  urunler: this.props.cartItems
                });
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    isAuth: state.R_Welcome.isAuth,
    cartItems: state.R_Cart.cartItems,
    loading: state.R_Cart.loading,
    error: state.R_Cart.error,
    totalPrice: state.R_Cart.totalPrice
  }),
  dispatch => ({
    actions: bindActionCreators(
      Object.assign({ getCartItems }, { updatePrice }, { deleteItem }),
      dispatch
    )
  })
)(S_Cart);

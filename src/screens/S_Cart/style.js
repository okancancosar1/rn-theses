import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  altCont: {
    flex: 1,

  },
  errorStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    height: 50,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#EEF0F3",
    borderColor: "pink",
    borderWidth: 1
  },
  fiyat: {
    flex: 1,
    flexDirection: "row",
    marginLeft: 10
  },
  price: {
    marginTop: 5,
    marginLeft: 10
  },
  btn: {
    borderColor: "transparent",
    borderWidth: 0,
    borderRadius: 5
  }
});

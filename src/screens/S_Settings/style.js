import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  personContain: {
    flex: 3,
    alignItems: "center",
    justifyContent: "center"
  },
  nameContain: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  menuContain: {
    flex: 7,
    flexDirection: "column",
    marginHorizontal: "20%",
    marginTop: 20
  },
  personStyle: {
    opacity: 0.5
  },
  iconView: {
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "black",
    opacity: 0.7
  },
  nameStyl: {
    fontSize: 18,
    fontWeight: "400"
  },
  buttonText: {
    marginLeft: 20,
    color: "black",
    opacity: 0.7,
    fontSize: 18
  },
  addStyle: {
    flexDirection: "row",
    marginVertical: 10
  }
});

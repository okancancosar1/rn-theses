import React, { Component } from "react";
import {
  View,
  Text,
  TouchableNativeFeedback,
  ToastAndroid
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Icon } from "react-native-elements";

import { Information } from "../../../Ortak";
import { styles } from "./style";
import Header from "../Common/Header";

import { signout } from "../../actions/A_Settings";

class S_Settings extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Header nav={this.props.navigation} />

        <View style={styles.personContain}>
          <View style={styles.iconView}>
            <Icon
              iconStyle={styles.personStyle}
              name="person"
              size={90}
              color="red"
            />
          </View>
        </View>

        <View style={styles.nameContain}>
          <Text style={styles.nameStyl}>
            {this.props.user.displayName
              ? this.props.user.displayName
              : "Hoşgeldiniz"}
          </Text>
        </View>

        <View style={styles.menuContain}>
          <TouchableNativeFeedback
            onPress={() => this.props.navigation.navigate("S_Orders")}
          >
            <View style={styles.addStyle}>
              <Icon
                iconStyle={styles.personStyle}
                name="history"
                size={35}
                color="red"
              />
              <Text style={styles.buttonText}>Önceki Siparişlerim</Text>
            </View>
          </TouchableNativeFeedback>

          <TouchableNativeFeedback
            onPress={() => {
              if (this.props.isAuth)
                this.props.navigation.navigate("S_UpdateProfile");
              else ToastAndroid.show("Önce giriş yapın.", ToastAndroid.SHORT);
            }}
          >
            <View style={styles.addStyle}>
              <Icon
                iconStyle={styles.personStyle}
                name="person"
                size={35}
                color="red"
              />
              <Text style={styles.buttonText}>Bilgilerim</Text>
            </View>
          </TouchableNativeFeedback>

          <TouchableNativeFeedback
            onPress={() => this.props.navigation.navigate("S_Adresses")}
          >
            <View style={styles.addStyle}>
              <Icon
                iconStyle={styles.personStyle}
                name="location-on"
                size={32}
                color="red"
              />
              <Text style={styles.buttonText}>Adreslerim</Text>
            </View>
          </TouchableNativeFeedback>

          <TouchableNativeFeedback
            onPress={() => this.props.navigation.navigate("S_Favourites")}
          >
            <View style={styles.addStyle}>
              <Icon
                iconStyle={styles.personStyle}
                name="favorite"
                size={32}
                color="red"
              />
              <Text style={styles.buttonText}>Favoriler</Text>
            </View>
          </TouchableNativeFeedback>

          {this.props.isAuth ? (
            <TouchableNativeFeedback
              onPress={() => {
                this.props.actions.signout(this.props.navigation);
              }}
            >
              <View style={styles.addStyle}>
                <Icon
                  iconStyle={styles.personStyle}
                  name="exit-to-app"
                  size={30}
                  color="red"
                />
                <Text style={styles.buttonText}>Çıkış</Text>
              </View>
            </TouchableNativeFeedback>
          ) : (
            <TouchableNativeFeedback
              onPress={() => {
                this.props.navigation.navigate("S_Auth");
              }}
            >
              <View style={styles.addStyle}>
                <Icon
                  name="all-inclusive"
                  size={30}
                  color="red"
                  iconStyle={styles.personStyle}
                />
                <Text style={styles.buttonText}>Giriş yap</Text>
              </View>
            </TouchableNativeFeedback>
          )}
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    user: state.R_Settings.user,
    isAuth: state.R_Settings.isAuth
  }),
  dispatch => ({
    actions: bindActionCreators(Object.assign({ signout }), dispatch)
  })
)(S_Settings);

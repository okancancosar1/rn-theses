import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  logoContainer: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center"
  },
  formContainer: {
    flex: 5
  },
  lastContainer: {
    flex: 1
  },

  logo: {
    height: 70,
    width: 70
  },
  inputCont: {
    flex: 4,
    marginHorizontal: width / 13
  },
  buttonCont: {
    flex: 1,
    marginHorizontal: width / 5,
    marginTop: 20
  },

  girisBtnCont: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 7
  },
  girisBtn: {
    flex: 1,
    fontSize: 13,
    opacity: 0.5
  }
});

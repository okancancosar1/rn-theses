import React, { Component } from "react";
import { TextInput, Text, View, Image, ActivityIndicator } from "react-native";
import {
  Button,
  FormValidationMessage,
  ButtonGroup
} from "react-native-elements";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import {
  updateButtonGroupIndex,
  emailChanged,
  passwordChanged,
  authUser
} from "../../actions/A_Auth";

import { styles } from "./style";
import { Information } from "../../../Ortak";
import { Separator, TouchDismiss } from "../Common";
import Header from "../Common/Header";

class S_Auth extends Component {
  createButton = () => {
    const {
      selectedButtonGroupIndex,
      email,
      password,
      navigation
    } = this.props;

    if (this.props.loading) {
      return <ActivityIndicator size="large" />;
    } else {
      if (this.props.selectedButtonGroupIndex == 0) {
        return (
          <Button
            style={styles.button}
            title="Giriş yap"
            color="#49505f"
            onPress={() => {
              this.props.actions.authUser(
                selectedButtonGroupIndex,
                email,
                password,
                navigation
              );
            }}
          />
        );
      } else if (this.props.selectedButtonGroupIndex == 1) {
        return (
          <Button
            style={styles.button}
            title="Kaydol"
            color="#49505f"
            onPress={() => {
              this.props.actions.authUser(
                selectedButtonGroupIndex,
                email,
                password,
                navigation
              );
            }}
          />
        );
      }
    }
  };

  render() {
    return (
      <TouchDismiss>
        <View style={styles.container}>
          <Header nav={this.props.navigation} />

          <View style={styles.logoContainer}>
            <Image style={styles.logo} source={Information.logo} />
          </View>

          <View style={styles.formContainer}>
            <View style={styles.inputCont}>
              <ButtonGroup
                onPress={this.props.actions.updateButtonGroupIndex}
                selectedIndex={this.props.selectedButtonGroupIndex}
                buttons={["Giriş Yap", "Kayıt Ol"]}
                containerStyle={{ height: 50, marginBottom: 50 }}
              />
              <TextInput
                value={this.props.email}
                placeholder={"E-Mail"}
                multiline={false}
                autoCorrect={false}
                autoCapitalize={"none"}
                keyboardType={"email-address"}
                returnKeyType={"next"}
                onSubmitEditing={() => this.input4.focus()}
                onChangeText={s => {
                  this.props.actions.emailChanged(s);
                }}
              />
              <FormValidationMessage>
                {this.props.emailError}
              </FormValidationMessage>
              <TextInput
                value={this.props.password}
                ref={a => (this.input4 = a)}
                placeholder={"Şifre"}
                returnKeyType={"done"}
                multiline={false}
                autoCorrect={false}
                autoCapitalize={"none"}
                secureTextEntry={true}
                onSubmitEditing={() => {
                  const {
                    selectedButtonGroupIndex,
                    email,
                    password,
                    navigation
                  } = this.props;
                  this.props.actions.authUser(
                    selectedButtonGroupIndex,
                    email,
                    password,
                    navigation
                  );
                }}
                onChangeText={s => {
                  this.props.actions.passwordChanged(s);
                }}
              />
              <FormValidationMessage>
                {this.props.passwordError}
              </FormValidationMessage>
              <View style={styles.buttonCont}>{this.createButton()}</View>
              <View style={styles.logoContainer}>
                <FormValidationMessage>
                  {this.props.error}
                </FormValidationMessage>
              </View>
            </View>
          </View>

          <View style={styles.lastContainer}>
            <View style={styles.girisBtnCont}>
              <Text
                onPress={() => this.props.navigation.navigate("S_ResetPass")}
                style={styles.girisBtn}
              >
                Şifrenizi mi unuttunuz?
              </Text>
            </View>
          </View>
        </View>
      </TouchDismiss>
    );
  }
}

export default connect(
  state => ({
    email: state.R_Auth.email,
    emailError: state.R_Auth.emailError,
    password: state.R_Auth.password,
    passwordError: state.R_Auth.passwordError,
    selectedButtonGroupIndex: state.R_Auth.selectedButtonGroupIndex,
    loading: state.R_Auth.loading,
    error: state.R_Auth.error
  }),
  dispatch => ({
    actions: bindActionCreators(
      Object.assign(
        { updateButtonGroupIndex },
        { emailChanged },
        { passwordChanged },
        { authUser }
      ),
      dispatch
    )
  })
)(S_Auth);

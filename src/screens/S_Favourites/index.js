import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FormValidationMessage } from "react-native-elements";

import { firebaselestirme } from "../../configs/initFirebase";
import { getAllFavs } from "../../actions/A_Favourites";
import { styles } from "./style";
import { Item } from "../S_Menu/Component";
import { Separator, Loading } from "../Common";
import Header from "../Common/Header";

class S_Favourites extends Component {
  componentDidMount() {
    if (this.props.isAuth) this.props.actions.getAllFavs();
  }

  renderFavItem = ({ item, index }) => (
    <Item
      comingFavs={true}
      item={item}
      onPress={() => {
        this.props.navigation.navigate("S_ProductDetail", {
          params: item.urunUID
        });
      }}
    />
  );

  render() {
    let favs = "";
    if (!this.props.isAuth) {
      favs = <Loading text="Favorilerinizi burada göreceksiniz..." />;
    } else {
      if (this.props.loading) {
        favs = <Loading icon text="Favorilerinizi Getiriyoruz." />;
      } else if (this.props.error) {
        favs = <Loading text={this.props.error} />;
      } else {
        favs = (
          <FlatList
            data={this.props.allFavs}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderFavItem}
          />
        );
      }
    }
    return (
      <View style={styles.container}>
        <Header nav={this.props.navigation} />
        <View>
          <Text style={{ margin: 5 }}> Favoriler </Text>
          <Separator />
        </View>
        {favs}
      </View>
    );
  }
}

export default connect(
  state => ({
    isAuth: state.R_Welcome.isAuth,
    allFavs: state.R_Favourites.allFavs,
    loading: state.R_Favourites.loading,
    error: state.R_Favourites.error
  }),
  dispatch => ({
    actions: bindActionCreators(Object.assign({ getAllFavs }), dispatch)
  })
)(S_Favourites);

import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  adresCont: {
    margin: 5
  },
  yontemCont: {
    flex: 2
  },
  notCont: {
    flex: 2
  },
  submitCont: {
    flex: 1
  },
  cartSitayl: {
    padding: 5,
    margin: 5
  }
});

import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  Picker,
  ScrollView,
  ActivityIndicator,
  ToastAndroid
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Button, Icon, CheckBox, Card } from "react-native-elements";

import {
  changeSelectedAddress,
  changeOrderNote,
  setOrder,
  reset
} from "../../actions/A_OrderDetail";
import { styles } from "./style";
import Header from "../Common/Header";
import { Separator } from "../Common";

class S_OrderDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nakit: true,
      kk: false
    };
  }
  // componentWillUnmount() {
  //   this.props.actions.reset();
  // }
  render() {
    return (
      <View style={styles.container}>
        <Header nav={this.props.navigation} />

        <View>
          <Text style={{ margin: 5 }}> Sipariş detaylarını ayarlayın </Text>
          <Separator />
        </View>
        <ScrollView>
          <Card title="Adres seçin" containerStyle={styles.cartSitayl}>
            <Picker
              selectedValue={this.props.selectedAddress}
              style={styles.picker}
              onValueChange={(val, index) => {
                this.props.actions.changeSelectedAddress(val);
              }}
            >
              <Picker.Item label={"Bir adres seçin. "} />
              {this.props.adresler.map((item, index) => {
                return (
                  <Picker.Item
                    key={Math.random().toString()}
                    label={`${item.baslik}, ${item.gercekSemt}`}
                    value={item}
                  />
                );
              })}
            </Picker>
          </Card>

          <Card title="Ödeme yöntemi seçin" containerStyle={styles.cartSitayl}>
            <CheckBox
              title="Nakit"
              checkedTitle="Nakit ✔"
              checkedIcon="check-circle"
              uncheckedIcon="circle"
              iconType="feather"
              checked={this.state.nakit}
              onPress={() => {
                const { nakit, kk } = this.state;
                if (nakit) {
                  this.setState({ nakit: false });
                  this.setState({ kk: true });
                } else {
                  this.setState({ nakit: true });
                  this.setState({ kk: false });
                }
              }}
            />
            <CheckBox
              title="Kapıda kredi kartı ile"
              checkedTitle="Kapıda kredi kartı ile  ✔"
              checkedIcon="check-circle"
              uncheckedIcon="circle"
              iconType="feather"
              checked={this.state.kk}
              onPress={() => {
                const { nakit, kk } = this.state;
                if (kk) {
                  this.setState({ nakit: true });
                  this.setState({ kk: false });
                } else {
                  this.setState({ nakit: false });
                  this.setState({ kk: true });
                }
              }}
            />
          </Card>

          <Card title="Sipariş notu" containerStyle={styles.cartSitayl}>
            <TextInput
              value={this.props.orderNote}
              placeholder={"Sipariş notunuz"}
              style={styles.inpt}
              multiline={true}
              numberOfLines={4}
              textAlignVertical={"top"}
              maxLength={120}
              underlineColorAndroid="transparent"
              autoCorrect={false}
              returnKeyType={"done"}
              onChangeText={s => {
                this.props.actions.changeOrderNote(s);
              }}
            />
          </Card>

          <Card containerStyle={styles.cartSitayl}>
            {this.props.loading ? (
              <ActivityIndicator size="large" color="#0f0" />
            ) : (
              <Button
                style={{ padding: 6 }}
                title="Siparişi tamamla"
                onPress={() => {
                  const { params } = this.props.navigation.state;

                  if (!this.props.selectedAddress) {
                    ToastAndroid.show(
                      "Adres seçmeyi ihmal etmeyin",
                      ToastAndroid.SHORT
                    );
                  } else if (this.props.minPrice > params.ucret) {
                    let fark = this.props.minPrice - params.ucret;
                    ToastAndroid.show(
                      `Minimum paket tutarının altındasınız.\nSiparişinizi tamamlamak için sepetinize ${fark.toFixed(
                        2
                      )} TL değerinde daha ürün eklemeniz `,
                      ToastAndroid.SHORT
                    );
                  } else {
                    const payload = {
                      adres: this.props.selectedAddress,
                      durum: "Oluşturuldu",
                      odemeYontemi: this.state.nakit ? "Nakit" : "Kredi Kartı",
                      siparisNotu: this.props.orderNote,
                      tarih: new Date().getTime(),
                      tutar: params ? params.ucret : 0,
                      urunler: params ? params.urunler : []
                    };
                    this.props.actions.setOrder(
                      payload,
                      this.props.navigation,
                      this.props.minTime
                    );
                  }
                }}
              />
            )}
          </Card>
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    adresler: state.R_Main.adresler,
    selectedAddress: state.R_OrderDetail.selectedAddress,
    minPrice: state.R_OrderDetail.minPrice,
    minTime: state.R_OrderDetail.minTime,
    orderNote: state.R_OrderDetail.orderNote,
    loading: state.R_OrderDetail.loading,
    error: state.R_OrderDetail.error
  }),
  dispatch => ({
    actions: bindActionCreators(
      Object.assign(
        { changeSelectedAddress },
        { changeOrderNote },
        { setOrder },
        { reset }
      ),
      dispatch
    )
  })
)(S_OrderDetail);

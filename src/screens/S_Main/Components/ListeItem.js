import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableWithoutFeedback } from "react-native";
import { Icon } from "react-native-elements";

export class ListeItem extends Component {
  shouldComponentUpdate() {
    return false;
  }
  render() {
    const { icon, text, type, prs } = this.props;
    return (
      <TouchableWithoutFeedback onPress={prs}>
        <View style={styles.container}>
          <View style={styles.contfirst}>
            <Icon style={styles.icon} name={icon} type={type} raised={false} />
            <Text style={styles.text}> {text} </Text>
          </View>
          <View style={styles.contArrow}>
            <Icon name={"chevron-right"} type={"evilicon"} raised={false} />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginHorizontal: 5,
    marginBottom: 10
  },
  contfirst: {
    flex: 3,
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  contArrow: {
    flex: 1
  },
  icon: {},
  text: {
    fontSize: 18,
    fontWeight: "400",
    marginLeft: 20
  }
});

import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  swipeCont: {
    flex: 1,
    margin: 5,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "pink",
    backgroundColor: "blue"
  },
  socialCont: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    borderRadius: 30,
    backgroundColor: "#d6d7da",
    marginHorizontal: 20
  },
  racentCont: {
    flex: 1,
    margin: 10,
    borderColor: "#000",
    borderRadius: 10,
    borderWidth: 1,
    paddingTop: 10
  },
  adressesCont: {
    flex: 1,
    marginVertical: 10
  },

  swipestyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  dot: {
    backgroundColor: "white",
    opacity: 0.5,
    width: 8,
    height: 8,
    borderRadius: 8,
    margin: 5
  },
  activeDot: {
    backgroundColor: "red",
    opacity: 0.2,
    width: 10,
    height: 10,
    borderRadius: 10,
    margin: 5
  },
  paginationStyle: {
    left: null,
    right: 10
  },

  image: {
    flex: 1,
    width
  },
  slide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  other: {
    flex: 2
  },
  adresyok: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 20
  },
  adresbaslik: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 15
  }
});

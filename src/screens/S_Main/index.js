import React, { Component } from "react";
import {
  Text,
  ToastAndroid,
  View,
  Image,
  ScrollView,
  FlatList,
  Linking
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Swiper from "react-native-swiper";
import { Icon, SocialIcon, ListItem } from "react-native-elements";

import { styles } from "./style";
import { ListeItem } from "./Components/ListeItem";
import { Separator, Loading } from "../Common";
import Header from "../Common/Header";

import { Information } from "../../../Ortak";
import { getAddresses } from "../../actions/A_Main";

class S_Main extends Component {
  componentDidMount() {
    if (this.props.isAuth) this.props.actions.getAddresses();
  }
  render() {
    let adresss = "";
    if (!this.props.isAuth) {
      adresss = <Loading text="Adresleriniz burada göreceksiniz..." />;
    } else {
      if (this.props.loading) {
        adresss = <Loading icon text="Adreslerinizi Getiriyoruz." />;
      } else if (this.props.error) {
        adresss = <Loading text={this.props.error} />;
      } else {
        adresss = (
          <FlatList
            data={this.props.adresler}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, i }) => {
              return (
                <ListItem
                  key={i}
                  title={item.gercekSemt}
                  subtitle={item.tumAdres}
                  leftIcon={{ name: item.tip }}
                  onPress={() => this.props.navigation.navigate("S_Adresses")}
                />
              );
            }}
          />
        );
      }
    }

    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
        <Header nav={this.props.navigation} />

        {/* Swipe view */}
        <View style={styles.swipeCont}>
          <Swiper
            style={styles.swipestyle}
            height={200}
            dot={<View style={styles.dot} />}
            activeDot={<View style={styles.activeDot} />}
            paginationStyle={styles.paginationStyle}
            autoplay={!__DEV__ ? true : false}
            autoplayTimeout={4}
            showsPagination
          >
            <View style={styles.slide}>
              <Image
                resizeMode="stretch"
                style={styles.image}
                source={require("./img/1.jpg")}
              />
            </View>
            <View style={styles.slide}>
              <Image
                resizeMode="stretch"
                style={styles.image}
                source={require("./img/2.jpg")}
              />
            </View>
            <View style={styles.slide}>
              <Image
                resizeMode="stretch"
                style={styles.image}
                source={require("./img/3.jpg")}
              />
            </View>
            <View style={styles.slide}>
              <Image
                resizeMode="stretch"
                style={styles.image}
                source={require("./img/4.jpg")}
              />
            </View>
          </Swiper>
        </View>

        {/* Social Medias view */}
        <View style={styles.socialCont}>
          <SocialIcon
            type="facebook"
            raised={false}
            iconSize={17}
            onPress={() => {
              Linking.openURL(Information.facebook);
            }}
          />
          <SocialIcon
            type="twitter"
            raised={false}
            iconSize={17}
            onPress={() => {
              Linking.openURL(Information.twitter);
            }}
          />
          <SocialIcon
            type="instagram"
            raised={false}
            iconSize={17}
            onPress={() => {
              Linking.openURL(Information.instagram);
            }}
          />
          <SocialIcon
            type="youtube"
            raised={false}
            iconSize={17}
            onPress={() => {
              Linking.openURL(Information.youtube);
            }}
          />
        </View>

        {/* Sık kullanılanlar */}
        <View style={styles.racentCont}>
          <Text style={{ marginLeft: 15 }}> Sık kullanılanlar </Text>
          <Separator />

          <ListItem
            key={Math.random().toString()}
            title={"Menüler"}
            leftIcon={{ name: "food", type: "material-community" }}
            onPress={() => this.props.navigation.navigate("S_Menu")}
          />

          <ListItem
            key={Math.random().toString()}
            title={"Son Sipariş Detayı"}
            leftIcon={{ name: "details", type: "material" }}
            onPress={() => {
              if (this.props.lastOrder) {
                this.props.navigation.navigate("S_OrderSuccess", {
                  newKey: this.props.lastOrder
                });
              } else {
                if (this.props.isAuth) {
                  ToastAndroid.show(
                    "Daha önce bir sipariş vermemişsiniz.",
                    ToastAndroid.SHORT
                  );
                } else {
                  ToastAndroid.show(
                    "Son sipariş detayınızı burada göreceksiniz. ",
                    ToastAndroid.SHORT
                  );
                }
              }
            }}
          />
          <ListItem
            key={Math.random().toString()}
            title={"Önceki Siparişler"}
            leftIcon={{ name: "history", type: "material-community" }}
            onPress={() => this.props.navigation.navigate("S_Orders")}
          />
          <ListItem
            key={Math.random().toString()}
            title={"Favoriler"}
            leftIcon={{ name: "favorite-border", type: "material" }}
            onPress={() => this.props.navigation.navigate("S_Favourites")}
          />
          <ListItem
            key={Math.random().toString()}
            title={"Ayarlar"}
            leftIcon={{ name: "settings", type: "simple-line-icon" }}
            onPress={() => this.props.navigation.navigate("S_Settings")}
          />
        </View>

        {/* Adresler */}
        <View style={styles.racentCont}>
          <View style={styles.adresbaslik}>
            <Text> Adreslerim </Text>
            <Icon
              name="ios-add-circle-outline"
              type="ionicon"
              size={26}
              iconStyle={{ marginTop: -5 }}
              onPress={() => {
                if (this.props.isAuth) {
                  this.props.navigation.navigate("S_NewAdress");
                }
              }}
            />
          </View>
          <Separator />
          <View style={styles.adressesCont}>{adresss}</View>
        </View>
      </ScrollView>
    );
  }
}

export default connect(
  state => ({
    isAuth: state.R_Welcome.isAuth,
    adresler: state.R_Main.adresler,
    error: state.R_Main.error,
    loading: state.R_Main.loading,
    lastOrder: state.R_Main.lastOrder
  }),
  dispatch => ({
    actions: bindActionCreators(Object.assign({ getAddresses }), dispatch)
  })
)(S_Main);

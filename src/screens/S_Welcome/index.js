import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";

import { styles } from "./style";
import { Information } from "../../../Ortak";
import { Separator } from "../Common";

import { checkAnyAuth } from "../../actions/A_Welcome";

class S_Welcome extends Component {
  componentDidMount() {
    this.props.actions.checkAnyAuth(this.props.navigation.navigate);
  }
  render() {
    if (this.props.showImage)
      return (
        <View style={styles.splashview}>
          <Image style={styles.splashimg} source={Information.logo} />
        </View>
      );
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image style={styles.logo} source={Information.logo} />
        </View>

        <View style={styles.secondContainer}>
          <View style={styles.markaCont}>
            <Text style={styles.marka}> {Information.name} </Text>
            <View style={styles.seperator} />
          </View>
          <View style={styles.buttonsCont}>
            <Button
              title="Aramıza katılın"
              onPress={() => this.props.navigation.navigate("S_Auth")}
            />
          </View>
          <Separator text={" Yada "} />
          <View style={styles.atlaCont}>
            <Text
              onPress={() => this.props.navigation.navigate("S_Main")}
              style={styles.atla}
            >
              Şimdilik atla >
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    isAuth: state.R_Welcome.isAuth,
    authError: state.R_Welcome.authError,
    showImage: state.R_Welcome.showImage,
    isOpen: state.R_Welcome.isOpen,
    isOpenError: state.R_Welcome.isOpenError
  }),
  dispatch => ({
    actions: bindActionCreators(Object.assign({ checkAnyAuth }), dispatch)
  })
)(S_Welcome);

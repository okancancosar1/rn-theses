import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  logoContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#D0F8E3"
  },
  secondContainer: {
    flex: 2
  },
  logo: {
    height: height / 5,
    width: width / 3
  },

  markaCont: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonsCont: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  atlaCont: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  marka: {
    fontSize: 40,
    fontFamily: "AmaticSC-Bold",
    color: "gray",
    marginBottom: 10
  },
  seperator: {
    height: 1,
    width: width / 3,
    justifyContent: "center",
    backgroundColor: "#818181",
    opacity: 0.5
  },
  atla: {
    flex: 1,
    color: "gray",
    marginTop: 25
  },
  splashview: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "black"
  },
  splashimg: {
    width: 150,
    height: 150
  }
});

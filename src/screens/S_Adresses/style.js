import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listCont: {
    marginHorizontal: 10
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    height: 50,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-around",
    backgroundColor: "#bceeff",
    borderRadius: 50,
    borderColor: "pink",
    borderWidth: 0.5
  },
  footertext: {
    fontSize: 20,
    opacity: 0.5,
    marginBottom: 5
  },
  centerez: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

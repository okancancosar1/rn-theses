import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Alert
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ListItem, Overlay } from "react-native-elements";

import { styles } from "./style";
import { Separator, Loading } from "../Common";
import Header from "../Common/Header";
import { deleteItem } from "../../actions/A_Adresses";

class S_Adresses extends Component {
  render() {
    let adrsler = "";
    let btn = <Text />;
    if (!this.props.isAuth) {
      adrsler = <Loading text="Adresleriniz burada gözükecek..." />;
    } else {
      btn = (
        <TouchableOpacity
          style={styles.footer}
          onPress={() => this.props.navigation.navigate("S_NewAdress")}
        >
          <Text style={styles.footertext}>Yeni Adres Ekle</Text>
        </TouchableOpacity>
      );
      if (this.props.loading) {
        adrsler = <Loading icon text="Adresleriniz getiriyoruz." />;
      } else {
        if (this.props.adresler.length == 0) {
          adrsler = <Loading text="Henüz bir adresiniz yok :(" />;
        } else {
          adrsler = (
            <View style={styles.listCont}>
              <FlatList
                data={this.props.adresler}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, i }) => {
                  return (
                    <ListItem
                      key={Math.random().toString()}
                      title={item.gercekSemt}
                      subtitle={item.tumAdres}
                      leftIcon={{ name: item.tip }}
                      onPress={() => {
                        console.log("::item.key::", item.key);
                        Alert.alert(
                          "",
                          "Adresi silmek istiyor musunuz?",
                          [
                            {
                              text: "Hayır",
                              onPress: () => console.log("Cancel Pressed"),
                              style: "cancel"
                            },
                            {
                              text: "Sil",
                              onPress: () =>
                                this.props.actions.deleteItem(item.key)
                            }
                          ],
                          { cancelable: true }
                        );
                      }}
                    />
                  );
                }}
              />
            </View>
          );
        }
      }
    }

    return (
      <View style={styles.container}>
        <Header nav={this.props.navigation} />
        <View>
          <Text style={{ margin: 5 }}> Adreslerim </Text>
          <Separator />
        </View>
        {adrsler}
        {btn}
      </View>
    );
  }
}

export default connect(
  state => ({
    isAuth: state.R_Welcome.isAuth,
    adresler: state.R_Main.adresler,
    loading: state.R_Main.loading,
    error: state.R_Main.error
  }),
  dispatch => ({
    actions: bindActionCreators(Object.assign({ deleteItem }), dispatch)
  })
)(S_Adresses);

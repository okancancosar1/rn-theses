import React, { Component } from "react";
import { TextInput, Text, Picker, View, ActivityIndicator } from "react-native";
import { Button, FormValidationMessage, Card } from "react-native-elements";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import {
  typeChanged,
  titleChanged,
  phoneChanged,
  semtChanged,
  adresChanged,
  directionsChanged,
  getSemt,
  saveAddres,
  resetReducers
} from "../../actions/A_NewAdress";

import { styles } from "./style";
import { Separator, TouchDismiss, MultiSelect } from "../Common";
import Header from "../Common/Header";

class S_NewAdress extends Component {
  componentWillMount() {
    this.props.actions.getSemt();
  }
  // componentWillUnmount() {
  //   this.props.actions.resetReducers();
  // }
  createButton = () => {
    if (this.props.loading) return <ActivityIndicator size="large" />;
    else
      return (
        <View>
          <Button
            style={styles.button}
            title="Adresi ekle"
            color="#49505f"
            onPress={() => {
              const {
                type,
                title,
                phone,
                semt,
                address,
                directions,
                navigation
              } = this.props;

              const returnstatement = {
                tip: type,
                baslik: title,
                telefon: phone,
                semtval: semt,
                tumAdres: address,
                tarifi: directions
              };
              this.props.actions.saveAddres(returnstatement, navigation);
            }}
          />
          <FormValidationMessage> {this.props.error} </FormValidationMessage>
        </View>
      );
  };

  render() {
    return (
      <TouchDismiss>
        <View style={styles.container}>
          <Header nav={this.props.navigation} />
          <Text style={styles.head}> Yeni Adres Ekle </Text>
          <Separator />

          <View style={styles.formContainer}>
            {/* adrestipi */}
            <View style={styles.pickerCont}>
              <Text style={styles.pickerText}> Adres tipi: </Text>
              <Picker
                ref={a => (this.picker1 = a)}
                selectedValue={this.props.type}
                style={styles.picker}
                mode={"dropdown"}
                onValueChange={(itemValue, itemIndex) => {
                  this.props.actions.typeChanged(itemValue);
                }}
              >
                <Picker.Item
                  key={Math.random().toString()}
                  label={"Bir adres tipi seçin. "}
                />
                <Picker.Item
                  key={Math.random().toString()}
                  label={"Ev"}
                  value={"home"}
                />
                <Picker.Item
                  key={Math.random().toString()}
                  label={"İş"}
                  value={"work"}
                />
                <Picker.Item
                  key={Math.random().toString()}
                  label={"Kampüs"}
                  value={"business"}
                />
                <Picker.Item
                  key={Math.random().toString()}
                  label={"Diğer"}
                  value={"assistant"}
                />
              </Picker>
            </View>

            {/* adres başlığı */}
            <View style={styles.pickerCont}>
              <Text style={styles.pickerText}> Adres başlığı: </Text>
              <TextInput
                ref={a => (this.input1 = a)}
                value={this.props.title}
                placeholder={"Adres başlığı"}
                style={styles.inpt}
                multiline={false}
                maxLength={20}
                autoCorrect={false}
                returnKeyType={"next"}
                onSubmitEditing={() => this.input2.focus()}
                onChangeText={s => {
                  this.props.actions.titleChanged(s);
                }}
              />
            </View>

            {/* cep telefon */}
            <View style={styles.pickerCont}>
              <Text style={styles.pickerText}> Cep telefonu: </Text>
              <TextInput
                ref={a => (this.input2 = a)}
                value={this.props.phone}
                placeholder={"Cep telefonu"}
                style={styles.inpt}
                multiline={false}
                maxLength={11}
                autoCorrect={false}
                keyboardType={"numeric"}
                returnKeyType={"next"}
                onSubmitEditing={() => this.input3.focus()}
                onChangeText={s => {
                  this.props.actions.phoneChanged(s);
                }}
              />
            </View>

            {/* semt */}
            <View style={styles.pickerCont}>
              <Text style={styles.pickerText}> Semt seç: </Text>
              <Picker
                selectedValue={this.props.semt}
                style={styles.picker}
                ref={a => (this.picker2 = a)}
                onValueChange={(itemValue, itemIndex) => {
                  this.props.actions.semtChanged(itemValue);
                }}
              >
                <Picker.Item label={"Bir semt seçin. "} />
                {this.props.tumSemtler.map((item, index) => {
                  return (
                    <Picker.Item
                      key={Math.random().toString()}
                      label={`${item.baslik}`}
                      value={`${item.key}.....${item.baslik}`}
                    />
                  );
                })}
              </Picker>
            </View>

            {/* adres */}
            <View style={styles.pickerCont}>
              <Text style={styles.pickerText}> Adres: </Text>
              <TextInput
                ref={a => (this.input3 = a)}
                value={this.props.address}
                placeholder={"Adres"}
                style={styles.inpt}
                multiline={true}
                numberOfLines={4}
                textAlignVertical={"top"}
                maxLength={120}
                autoCorrect={false}
                returnKeyType={"next"}
                onSubmitEditing={() => this.input4.focus()}
                onChangeText={s => {
                  this.props.actions.adresChanged(s);
                }}
              />
            </View>

            {/* Adres tarifi */}
            <View style={styles.pickerCont}>
              <Text style={styles.pickerText}> Adres tarifi: </Text>
              <TextInput
                ref={a => (this.input4 = a)}
                value={this.props.directions}
                placeholder={"Adres tarifi"}
                style={styles.inpt}
                multiline={true}
                numberOfLines={4}
                textAlignVertical={"top"}
                maxLength={120}
                autoCorrect={false}
                returnKeyType={"done"}
                onChangeText={s => {
                  this.props.actions.directionsChanged(s);
                }}
              />
            </View>

            <View style={styles.buttonCont}>{this.createButton()}</View>
          </View>
        </View>
      </TouchDismiss>
    );
  }
}

export default connect(
  state => ({
    isAuth: state.R_Welcome.isAuth,
    type: state.R_NewAdress.type,
    title: state.R_NewAdress.title,
    phone: state.R_NewAdress.phone,
    semt: state.R_NewAdress.semt,
    address: state.R_NewAdress.address,
    directions: state.R_NewAdress.directions,
    tumSemtler: state.R_NewAdress.tumSemtler,
    loading: state.R_NewAdress.loading,
    error: state.R_NewAdress.error
  }),
  dispatch => ({
    actions: bindActionCreators(
      Object.assign(
        { typeChanged },
        { titleChanged },
        { phoneChanged },
        { semtChanged },
        { adresChanged },
        { directionsChanged },
        { getSemt },
        { resetReducers },
        { saveAddres }
      ),
      dispatch
    )
  })
)(S_NewAdress);

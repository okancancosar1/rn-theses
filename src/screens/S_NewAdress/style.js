import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  formContainer: {
    flex: 4,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 10
  },
  head: {
    margin: 5
  },
  inpt: {
    flex: 1,
    marginLeft: -100
  },
  buttonCont: {
    flex: 1,
    marginHorizontal: width / 5,
    marginTop: 30
  },

  girisBtnCont: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 7
  },
  girisBtn: {
    flex: 1,
    fontWeight: "bold",
    fontSize: 16,
    opacity: 0.5,
    marginTop: 15
  },
  pickerCont: {
    flexDirection: "row",
    marginBottom: 10
  },
  pickerText: {
    flex: 1
  },
  picker: {
    flex: 2,
    marginTop: -11
  }
});

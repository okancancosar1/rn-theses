import { StyleSheet, Dimensions, StatusBar } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  menuCont: {
    flex: 1,
    flexDirection: "row"
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  ustmenuCont: {
    flex: 1,
    backgroundColor: "#EDEDEE",
    borderColor: "pink",
    borderRightWidth: 1
  },
  foodCont: {
    flex: 5
  },
  menuTextStyle: {
    transform: [{ rotate: "-40deg" }]
  },
  menubaslik: {
    fontWeight: "bold",
    marginTop: 15,
    marginLeft: 5
  },
  selectedCont: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f9dbe6",
    borderColor: "pink",
    borderBottomWidth: 1
  },
  Cont: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "pink",
    borderBottomWidth: 1
  },
  topMenuCont: {
    borderColor: "pink",
    borderBottomWidth: 1
  }
});

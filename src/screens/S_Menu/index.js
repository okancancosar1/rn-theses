import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableNativeFeedback
} from "react-native";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { firebaselestirme } from "../../configs/initFirebase";
import { getAllMenus, setSelectedMenu } from "../../actions/A_Menu";
import { styles } from "./style";
import { Item } from "./Component";
import { Separator } from "../Common";
import Header from "../Common/Header";

class S_Menu extends Component {
  componentDidMount() {
    this.props.actions.getAllMenus();
  }

  renderMenuContentItem = ({ item, index }) => (
    <Item
      item={item}
      onPress={() => {
        this.props.navigation.navigate("S_ProductDetail", {
          params: item.key
        });
      }}
    />
  );

  render() {
    if (this.props.menuNames && !this.props.loading) {
      if (this.props.menuNames[this.props.selectedMenu]) {
        const data = this.props.menuNames[this.props.selectedMenu];

        const menuDetayData = firebaselestirme(data.menuUrunleri);

        return (
          <View style={styles.container}>
            {/* Header */}
            <Header nav={this.props.navigation} />

            <View style={styles.menuCont}>
              {/* soldaki üst menu adları  */}
              <View style={styles.ustmenuCont}>
                <Text style={styles.menubaslik}>Menüler</Text>
                <Separator />

                {/* Soldaki üst menüler */}
                {this.props.menuNames.map((item, index) => (
                  <TouchableNativeFeedback
                    keyExtractor={(item, index) => index.toString()}
                    style={styles.topMenuCont}
                    onPress={() => {
                      this.props.actions.setSelectedMenu(index);
                    }}
                  >
                    <View
                      style={
                        this.props.selectedMenu == index
                          ? styles.selectedCont
                          : styles.Cont
                      }
                    >
                      <Text
                        keyExtractor={(item, index) => index.toString()}
                        style={styles.menuTextStyle}
                      >
                        {item.kisaBaslik}
                      </Text>
                    </View>
                  </TouchableNativeFeedback>
                ))}
              </View>
              {/* Menu ürünleri  */}
              <View style={styles.foodCont}>
                <Text style={styles.menubaslik}> ## {data.baslik}</Text>
                <Separator />
                {/* sağdaki menu detayları */}
                <FlatList
                  data={menuDetayData}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={this.renderMenuContentItem}
                />
              </View>
            </View>
          </View>
        );
      } else return <ActivityIndicator size="large" />;
    } else {
      return (
        <View style={styles.container}>
          {/* Header */}
          <Header nav={this.props.navigation} />

          <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" />
            <Text> Menuleri Getiriyoruz </Text>
          </View>
        </View>
      );
    }
  }
}

export default connect(
  state => ({
    selectedMenu: state.R_Menu.selectedMenu,
    product: state.R_Menu.product,
    loading: state.R_Menu.loading,
    error: state.R_Menu.error,
    menuNames: state.R_Menu.menuNames
  }),
  dispatch => ({
    actions: bindActionCreators(
      Object.assign({ setSelectedMenu }, { getAllMenus }),
      dispatch
    )
  })
)(S_Menu);

/*{
    list.map((item, i) => (
      <ListItem
        key={i}
        title={item.title}
        leftIcon={{ name: item.icon }}
      />
    ))
  }*/

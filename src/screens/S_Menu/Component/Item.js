import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableNativeFeedback
} from "react-native";
const { height, width } = Dimensions.get("window");

import { Icon } from "react-native-elements";
import { Separator } from "../../Common";

class Item extends Component {
  render() {
    const { item, onPress, comingFavs } = this.props;
    let iconName = "add";
    comingFavs ? (iconName = "favorite") : (iconName = "add");

    return (
      <TouchableNativeFeedback onPress={onPress}>
        <View style={styles.container}>
          <View style={styles.row}>
            <View style={styles.addCont}>
              <Icon
                iconStyle={styles.addictionalIconStyle}
                name={iconName}
                size={32}
                color={"black"}
              />
            </View>
            <View style={styles.infoCont}>
              <Text style={styles.productName}> {item.ad} </Text>
              <Text style={styles.productExp} numberOfLines={2}>
                {item.aciklama}
              </Text>
            </View>
            <View style={styles.priceCont}>
              <Icon
                type={"font-awesome"}
                iconStyle={styles.tryIconStyle}
                name={"try"}
                size={15}
                color={"gray"}
              />
              <Text style={styles.price}> {item.fiyat}</Text>
            </View>
          </View>
          <Separator />
        </View>
      </TouchableNativeFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginRight: 10,
    flex: 1
  },
  row: {
    flex: 1,
    flexDirection: "row"
  },
  addCont: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  infoCont: {
    flex: 5
  },
  priceCont: {
    flex: 1,
    flexDirection: "row"
  },
  productName: {
    color: "red",
    opacity: 0.9,
    fontSize: 13
  },
  productExp: {
    fontSize: 12,
    marginVertical: 5,
    opacity: 0.5
  },
  price: {
    flex: 1,
    opacity: 0.5,
    color: "red",
    fontSize: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  addictionalIconStyle: {
    opacity: 0.5,
    color: "red",
    marginRight: 5,
    borderRightColor: "red",
    borderRightWidth: StyleSheet.hairlineWidth
  },
  tryIconStyle: {
    flex: 1,
    opacity: 0.5,
    color: "red",
    alignItems: "center",
    justifyContent: "center"
  }
});

export { Item };

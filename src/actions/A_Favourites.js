import firebase from "firebase";
import _ from "lodash";
import { firebaselestirme } from "../configs/initFirebase";

export const types = {
  ALL_FAV: "Tüm favlar getirilecek",
  ALL_FAV_SUCCESS: "Tüm favlar alındı",
  ALL_FAV_FAIL: "Tüm favlar çekilemedi"
};

/**
 * Tüm favoriler geliyor
 */
export const getAllFavs = () => {
  return dispatch => {
    getAllFavs1(dispatch);
  };
};
const getAllFavs1 = dispatch => {
  dispatch({ type: types.ALL_FAV });
  const userId = firebase.auth().currentUser.uid;

  try {
    firebase
      .database()
      .ref(`/firma1/uyeler/${userId}/favoriler`)
      .on("value", snapshot => {
        if (snapshot.val()) {
          const allfav = firebaselestirme(snapshot.val());
          dispatch({ type: types.ALL_FAV_SUCCESS, payload: allfav });
        } else
          dispatch({
            type: types.ALL_FAV_FAIL,
            payload: "Henüz bir favoriniz yok :("
          });
      });
  } catch (er) {
    dispatch({ type: types.ALL_FAV_FAIL, payload: er });
  }
};

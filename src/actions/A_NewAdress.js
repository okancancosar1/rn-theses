import firebase from "firebase";
import { ToastAndroid } from "react-native";

import { firebaselestirme } from "../configs/initFirebase";

export const types = {
  TYPE_CHANGED: "Adres tipi inputu değiştirme",
  TITLE_CHANGED: "Adres başlığı inputu değiştirme",
  PHONE_CHANGED: "Adres telefonu inputu değiştirme",
  SEMT_CHANGED: "Adres semt inputu değiştirme",
  ADRESS_CHANGED: "Adres inputu değiştirme",
  DIRECTIONS_CHANGED: "Adres tarifi inputu değiştirme",

  GETSEMT: "Semtler getiriliyor.",

  ADD_ADDRESS: "Kayıt işlemi başladı.",
  ADD_ADDRESS_SUCCESS: "Kayıt başarılı",
  ADD_ADDRESS_FAIL: "Kayıt başarısız oldu",

  RESET: "reducers reset"
};

/**
 * type input change
 */
export const typeChanged = type => {
  return dispatch => {
    dispatch({ type: types.TYPE_CHANGED, payload: type });
  };
};

/**
 * title input change
 */
export const titleChanged = title => {
  return dispatch => {
    dispatch({ type: types.TITLE_CHANGED, payload: title });
  };
};

/**
 * phone input change
 */
export const phoneChanged = phone => {
  return dispatch => {
    dispatch({ type: types.PHONE_CHANGED, payload: phone });
  };
};

/**
 * semt input change
 */
export const semtChanged = semt => {
  return dispatch => {
    dispatch({ type: types.SEMT_CHANGED, payload: semt });
  };
};

/**
 * adres input change
 */
export const adresChanged = address => {
  return dispatch => {
    dispatch({ type: types.ADRESS_CHANGED, payload: address });
  };
};

/**
 * Adres tarifi input change
 */
export const directionsChanged = directions => {
  return dispatch => {
    dispatch({ type: types.DIRECTIONS_CHANGED, payload: directions });
  };
};
/**
 * Spinnerda basılmak üzere kayıtlı semtleri getirir.
 */
export const getSemt = () => {
  return dispatch => {
    getSemt1(dispatch);
  };
};
const getSemt1 = async dispatch => {
  return await firebase
    .database()
    .ref("firma1/subeler/sube1/subeBilgileri/semtler")
    .once("value", snaphot => {
      const safe = firebaselestirme(snaphot.val());
      dispatch({ type: types.GETSEMT, payload: safe });
    });
};

/**
 * alınan adresi kaydeder.
 */
export const saveAddres = (returnstatement, navigation) => {
  return dispatch => {
    saveAddres1(dispatch, returnstatement, navigation);
  };
};
const saveAddres1 = async (dispatch, returnstatement, navigation) => {
  dispatch({ type: types.ADD_ADDRESS });
  const userId = firebase.auth().currentUser.uid;
  let hata = "";

  console.log("::returnstatemenr::", returnstatement);
  if (returnstatement.baslik == "") {
    hata = "Adres başlığı yazmalısınız.";
    dispatch({ type: types.ADD_ADDRESS_FAIL, payload: hata });
    return;
  }
  if (returnstatement.tip == "") {
    hata = "Adres tipi seçmelisiniz.";
    dispatch({ type: types.ADD_ADDRESS_FAIL, payload: hata });
    return;
  }
  if (returnstatement.telefon == "") {
    hata = "Telefon yazmalısınız.";
    dispatch({ type: types.ADD_ADDRESS_FAIL, payload: hata });
    return;
  }
  if (returnstatement.semtval == undefined || returnstatement.semtval == "") {
    hata = "Semt seçmelisiniz.";
    dispatch({ type: types.ADD_ADDRESS_FAIL, payload: hata });
    return;
  }
  if (returnstatement.tumAdres == "") {
    hata = "Adres yazmalısınız.";
    dispatch({ type: types.ADD_ADDRESS_FAIL, payload: hata });
    return;
  }

  returnstatement.semtUID = returnstatement.semtval.split(".....")[0];
  returnstatement.gercekSemt = returnstatement.semtval.split(".....")[1];

  try {
    return await firebase
      .database()
      .ref(`firma1/uyeler/${userId}/adresler`)
      .push(returnstatement)
      .then(() => {
        dispatch({ type: types.ADD_ADDRESS_SUCCESS });
        ToastAndroid.show("Yeni bir adres eklediniz.", ToastAndroid.SHORT);
        navigation.navigate("S_Adresses");
      });
  } catch (er) {
    dispatch({ type: types.ADD_ADDRESS_FAIL, payload: er });
  }
};

export const resetReducers = () => {
  return dispatch => {
    dispatch({ type: types.RESET });
  };
};

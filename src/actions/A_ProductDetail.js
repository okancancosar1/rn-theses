import firebase from "firebase";
import { ToastAndroid } from "react-native";
import { firebaselestirme } from "../configs/initFirebase";
import _ from "lodash";

export const types = {
  GET_PRODUCT_DETAIL: "Ürün detayları getirlmesi başladı",
  GET_PRODUCT_DETAIL_SUCCESS: "Ürün detayları getirlmesi tamamlandı.",
  GET_PRODUCT_DETAIL_FAIL: "Ürün detayları getirlmesi hatalı",

  PRODUCT_COUNT_INCREASE: "Ürün sayısını arttı.",
  PRODUCT_COUNT_DECREASE: "Ürün sayısını azaldı.",

  CHANGE_EXTRA_SELECTED: "Ek ürünlerden seçilenler dizisi güncellendi.",
  CHANGE_DRINK_SELECTED: "Ek içeceklerden seçilen değer güncellendi.",

  CHANGE_PRODUCT_PRICE: "Ürün fiyatı değişti",
  CHANGE_DRINK_PRICE: "İçecek seçimi fiyatı değişti.",
  CHANGE_TOTAL_PRICE: "Genel toplam değişti.",
  CHANGE_EXTRAS_PRICE: "Extraların tutarları değişti.",

  ADD_TO_CART: "Sepete ekleme başladı",
  ADD_TO_CART_SUCCESS: "Sepete ekleme başarılı",
  ADD_TO_CART_FAIL: "Sepete ekleme hata",

  RESET_REDUCERS: "Reducer itemleri sıfırlandı.",

  CHECK_IS_FAV: "Ürün favorilerde mi.."
};

export const getProductDetail = key => {
  return dispatch => {
    getProductDetail1(dispatch, key);
  };
};
const getProductDetail1 = async (dispatch, key) => {
  dispatch({ type: types.GET_PRODUCT_DETAIL });
  try {
    return await firebase
      .database()
      .ref(`/firma1/subeler/sube1/menuler/`)
      .once("value", snap => {
        if (snap.val()) {
          const all = firebaselestirme(snap.val());
          all.forEach(element => {
            let lettit = firebaselestirme(element.menuUrunleri);
            lettit.forEach(item => {
              if (item.key == key) {
                return dispatch({
                  type: types.GET_PRODUCT_DETAIL_SUCCESS,
                  payload: item
                });
              }
            });
          });
        }
      });
  } catch (er) {
    dispatch({ type: types.GET_PRODUCT_DETAIL_FAIL, payload: er });
  }
};

/**
 * Ürün sayısı arttırma
 */
export const productCountIncrease = count => {
  return dispatch => {
    dispatch({ type: types.PRODUCT_COUNT_INCREASE, payload: count });
  };
};

/**
 * Ürün sayısı AZALTMA
 */
export const productCountDecrease = count => {
  return dispatch => {
    dispatch({ type: types.PRODUCT_COUNT_DECREASE, payload: count });
  };
};

/**
 * extra malzemeler kısmından seçilen elemanları günceller
 */
export const changeExtraSelectedItems = item => {
  return dispatch => {
    dispatch({ type: types.CHANGE_EXTRA_SELECTED, payload: item });
  };
};

/**
 * içeceklerden seçilen elemanı ekler
 */
export const changeDrinkSelectedItems = item => {
  return dispatch => {
    dispatch({ type: types.CHANGE_DRINK_SELECTED, payload: item });
  };
};

export const updatePrice = (item, value) => {
  return dispatch => {
    if (item == "productPrice")
      dispatch({ type: types.CHANGE_PRODUCT_PRICE, payload: value });
    else if (item == "drinkPrice")
      dispatch({ type: types.CHANGE_DRINK_PRICE, payload: value });
    else if (item == "total")
      dispatch({ type: types.CHANGE_TOTAL_PRICE, payload: value });
    else if (item == "extraPrice")
      dispatch({ type: types.CHANGE_EXTRAS_PRICE, payload: value });
  };
};

export const addToCart = (allData, navigation) => {
  return dispatch => {
    addToCart1(dispatch, allData, navigation);
  };
};
const addToCart1 = async (dispatch, allData, navigation) => {
  const userId = firebase.auth().currentUser.uid;
  dispatch({ type: types.ADD_TO_CART });
  try {
    return await firebase
      .database()
      .ref(`firma1/uyeler/${userId}/sepet`)
      .push(allData)
      .then(() => {
        console.log("::ekleme::başarılı");
        dispatch({ type: types.ADD_TO_CART_SUCCESS });
        ToastAndroid.show("Ürün sepetinize eklendi.", ToastAndroid.SHORT);
        // navigation.navigate("S_Cart");
      });
  } catch (er) {
    dispatch({ type: types.ADD_TO_CART_FAIL, payload: er });
  }
};

export const resetReducer = () => {
  return dispatch => {
    dispatch({ type: types.RESET_REDUCERS });
  };
};

/**
 * ürün favoriler listeside mi
 */
export const checkIsFav = key => {
  return dispatch => {
    checkIsFav1(dispatch, key);
  };
};
const checkIsFav1 = async (dispatch, key) => {
  const userId = firebase.auth().currentUser.uid;

  return await firebase
    .database()
    .ref(`firma1/uyeler/${userId}/favoriler`)
    .on("value", s => {
      if (s.val()) {
        const allfavs = firebaselestirme(s.val());

        dispatch({ type: types.CHECK_IS_FAV, payload: "" });

        allfavs.forEach(element => {
          if (element.urunUID == key)
            dispatch({ type: types.CHECK_IS_FAV, payload: element.key });
        });
      }
    });
};

/**
 * ürün favoriler listesine ekler
 */
export const addToFav = (key, ad, aciklama, fiyat) => {
  return dispatch => {
    addToFav1(dispatch, key, ad, aciklama, fiyat);
  };
};
const addToFav1 = (dispatch, key, ad, aciklama, fiyat) => {
  const userId = firebase.auth().currentUser.uid;

  const as = {
    urunUID: key,
    ad,
    aciklama,
    fiyat
  };

  return firebase
    .database()
    .ref(`firma1/uyeler/${userId}/favoriler/`)
    .push(as)
    .then(a => {
      dispatch({ type: types.CHECK_IS_FAV, payload: a.key });
    });
};

/**
 * ürün favoriler listesiden siler
 */
export const rmToFav = key => {
  return dispatch => {
    rmToFav1(dispatch, key);
  };
};
const rmToFav1 = (dispatch, key) => {
  const userId = firebase.auth().currentUser.uid;

  return firebase
    .database()
    .ref(`firma1/uyeler/${userId}/favoriler/${key}`)
    .remove()
    .then(() => {
      dispatch({ type: types.CHECK_IS_FAV, payload: "" });
    });
};

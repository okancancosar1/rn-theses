import firebase from "firebase";

export const deleteItem = key => {
  return dispatch => {
    deleteItem1(dispatch, key);
  };
};
const deleteItem1 = (dispatch, key) => {
  const userId = firebase.auth().currentUser.uid;
  return firebase
    .database()
    .ref(`/firma1/uyeler/${userId}/adresler/${key}`)
    .remove();
};

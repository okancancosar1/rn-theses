import firebase from "firebase";
import { ToastAndroid } from "react-native";
import { NavigationActions } from "react-navigation";

export const types = {
  ISAUTHCHECK: "Herhangi bir giriş var mı kontrolü başladı.",
  ISAUTHCHECK_SUCCESS: "Giriş var",
  ISAUTHCHECK_SUCCESS2: "Giriş YOK",
  ISAUTHCHECK_FAIL: "Herhangi bir giriş var mı kontrolü başarısız oldu"
};

export const checkAnyAuth = navigation => {
  return dispatch => {
    checkPerson(dispatch, navigation);
  };
};

const checkPerson = async (dispatch, navigation) => {
  dispatch({ type: types.ISAUTHCHECK });

  try {
    await firebase.auth().onAuthStateChanged(user => {
      if (user) {
        dispatch({ type: types.ISAUTHCHECK_SUCCESS, payload: user });
      } else {
        console.log("::onAuthStateChanged:: No user is signed in.");
        dispatch({ type: types.ISAUTHCHECK_SUCCESS2 });
      }
    });
  } catch (er) {
    dispatch({ type: types.ISAUTHCHECK_FAIL, payload: er });
  }
};

export const signout = navigation => {
  return dispatch => {
    signout1(dispatch, navigation);
  };
};

const signout1 = async (dispatch, navigation) => {
  await firebase
    .auth()
    .signOut()
    .then(() => {
      ToastAndroid.show("Daha sonra görüşmek dileğiyle", ToastAndroid.SHORT);
      dispatch({ type: "USER_LOGOUT" });
    })
    .catch(error => {
      ToastAndroid.show("Çıkış hata", ToastAndroid.SHORT);
    });
};

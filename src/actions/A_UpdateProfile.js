import firebase from "firebase";
import _ from "lodash";
import validate from "validate.js";
import { Alert } from "react-native";

export const types = {
  NAME_CHANGED: "isim inputu değiştirme",
  SURNAME_CHANGED: "soyisim inputu değiştirme",
  PHONE_CHANGED: "telefon inputu değiştirme",

  UPDATE: "Güncelleme işlemi başladı.",
  UPDATE_SUCCESS: "Güncelleme başarılı",
  UPDATE_FAIL: "Güncelleme başarısız oldu"
};

/**
 * name input change
 */
export const nameChanged = name => {
  return dispatch => {
    dispatch({ type: types.NAME_CHANGED, payload: name });
  };
};

/**
 * surname input change
 */
export const surnameChanged = surname => {
  return dispatch => {
    dispatch({ type: types.SURNAME_CHANGED, payload: surname });
  };
};

/**
 * phone input change
 */
export const phoneChanged = phone => {
  return dispatch => {
    dispatch({ type: types.PHONE_CHANGED, payload: phone });
  };
};

/**
 * Güncelleme işlemi
 */
export const updateUser = (name, surname, phone, navigation) => {
  return dispatch => {
    UPUSER(dispatch, name, surname, phone, navigation);
  };
};

const UPUSER = async (dispatch, name, surname, phone, navigation) => {
  dispatch({ type: types.UPDATE });
  const user = firebase.auth().currentUser;
  if (user) {
    user.updateProfile({
      displayName: `${name} ${surname}`,
      phoneNumber: phone
      // photoURL: ""
    });
    console.log("::A_UPUSER::", user.uid);
    try {
      await firebase
        .database()
        .ref("firma1/uyeler/" + user.uid)
        .set({
          ad: name,
          soyad: surname,
          telefon: phone
        })
        .then(() => {
          dispatch({ type: types.UPDATE_SUCCESS });
          navigation.navigate("S_Main");
        });
    } catch (er) {
      dispatch({ type: types.UPDATE_FAIL, payload: er });
    }
  }
};

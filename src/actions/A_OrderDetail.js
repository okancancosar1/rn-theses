import firebase from "firebase";
import { firebaselestirme } from "../configs/initFirebase";

export const types = {
  CHANGED_SELECTED_ADDRESS: "Seçili adres değişti.",
  CHANGED_MIN_PRICE: "Adrese min tutar değişti",
  CHANGED_MIN_TIME: "Adrese min ulaşma zamanı değişti",

  ORDER_NOTE_CHANGED: "Sipariş notu değişti.",

  SET_ORDER: "Sipariş oluşturulma başladı.",
  SET_ORDER_SUCCESS: "Sipariş oluşturulma tamamlandı.",
  SET_ORDER_FAIL: "Sipariş oluşturulma HATA.",

  CLEAR_CART: "Sepet temizlendi.",
  ADD_ORDER_2_PROFİLE: "Sipariş profile eklendi.",

  RESET: "Reset reducers"
};

/**
 * kullanıcının seçtiği adres değişti.
 */
export const changeSelectedAddress = payload => {
  return dispatch => {
    getAddressDetail(dispatch, payload);
  };
};

const getAddressDetail = (dispatch, payload) => {
  dispatch({ type: types.CHANGED_SELECTED_ADDRESS, payload });
  return firebase
    .database()
    .ref(`firma1/subeler/sube1/subeBilgileri/semtler/${payload.semtUID}/`)
    .once("value", snapshot => {
      const data = snapshot.val();
      if (data) {
        dispatch({ type: types.CHANGED_MIN_PRICE, payload: data.minUcret });
        dispatch({ type: types.CHANGED_MIN_TIME, payload: data.minZaman });
      }
    });
};

/**
 *  Sipariş notu değişti.
 */
export const changeOrderNote = payload => {
  return dispatch => {
    dispatch({ type: types.ORDER_NOTE_CHANGED, payload });
  };
};

/**
 * Sipariş oluşturma
 */
export const setOrder = (payload, navigation, time) => {
  return dispatch => {
    setOrder1(dispatch, payload, navigation, time);
  };
};
const setOrder1 = async (dispatch, payload, navigation, time) => {
  const userId = firebase.auth().currentUser.uid;
  dispatch({ type: types.SET_ORDER });

  // Get a key for a new Post.
  var newKey = await firebase
    .database()
    .ref("firma1/subeler/sube1/")
    .child("siparisler")
    .push().key;

  payload.kisiUID = userId;

  try {
    await firebase
      .database()
      .ref(`firma1/subeler/sube1/siparisler/${newKey}`)
      .update(payload)
      .then(() => {
        deleteCart(dispatch);
        addToProfile(dispatch, payload, newKey);
        dispatch({ type: types.SET_ORDER_SUCCESS });
        navigation.navigate("S_OrderSuccess", { time, newKey });
      });
  } catch (error) {
    dispatch({ type: types.SET_ORDER_FAIL, payload: error });
  }
};
const deleteCart = async dispatch => {
  const userId = firebase.auth().currentUser.uid;

  return await firebase
    .database()
    .ref(`firma1/uyeler/${userId}/sepet`)
    .remove()
    .then(() => {
      dispatch({ type: types.CLEAR_CART });
    });
};
const addToProfile = async (dispatch, payload, key) => {
  const userId = firebase.auth().currentUser.uid;

  return await firebase
    .database()
    .ref(`firma1/uyeler/${userId}/oncekiSiparisler/${key}`)
    .update(payload)
    .then(() => {
      dispatch({ type: types.ADD_ORDER_2_PROFİLE });
    });
};

export const reset = () => {
  return dispatch => {
    dispatch({ type: types.RESET });
  };
};

import firebase from "firebase";
import { firebaselestirme } from "../configs/initFirebase";

export const types = {
  GETTING_CART_START: "Sepet getiriliyor.",
  GETTING_CART_SUCCESS: "Sepet geldi",
  GETTING_CART_FAIL: "Sepet gelirken hata",

  UPDATE_TOTAL: "Sepet fiyat güncellemesi",

  DELETE_ITEM: "Ürün sepetten silindi."
};

export const getCartItems = () => {
  return dispatch => {
    getCartItems1(dispatch);
  };
};

const getCartItems1 = async dispatch => {
  dispatch({ type: types.GETTING_CART_START });
  const userId = firebase.auth().currentUser.uid;

  try {
    await firebase
      .database()
      .ref(`/firma1/uyeler/${userId}/sepet/`)
      .on("value", snapshot => {
        if (snapshot.val()) {
          const array = firebaselestirme(snapshot.val());
          dispatch({ type: types.GETTING_CART_SUCCESS, payload: array });

          let toplam = 0;
          array.forEach(element => {
            toplam += element.fiyat;
          });

          dispatch({ type: types.UPDATE_TOTAL, payload: toplam });
        } else
          dispatch({
            type: types.GETTING_CART_FAIL,
            payload: "Sepetiniz henüz boş :("
          });
      });
  } catch (error) {
    dispatch({ type: types.GETTING_CART_FAIL, payload: error });
  }
};

export const updatePrice = async (dispatch, price) => {
  return dispatch => {
    dispatch({ type: types.UPDATE_TOTAL, payload: price });
  };
};

export const deleteItem = key => {
  return dispatch => {
    deleteItem1(dispatch, key);
  };
};
const deleteItem1 = (dispatch, key) => {
  const userId = firebase.auth().currentUser.uid;
  return firebase
    .database()
    .ref(`/firma1/uyeler/${userId}/sepet/${key}`)
    .remove();
};

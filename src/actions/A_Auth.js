import firebase from "firebase";
import { ToastAndroid } from "react-native";
import validate from "validate.js";

export const types = {
  ButtonGroup_CHANGED: "kayıt mı giriş mi buton grubu değişti.",

  EMAIL_CHANGED: "email inputu değiştirme",
  PASSWORD_CHANGED: "şifre inputu değiştirme",

  REGISTER: "Kayıt işlemi başladı.",
  REGISTER_SUCCESS: "Kayıt başarılı",
  REGISTER_FAIL: "Kayıt başarısız oldu",

  LOGIN: "Giriş işlemi başladı.",
  LOGIN_SUCCESS: "Giriş başarılı",
  LOGIN_FAIL: "Giriş başarısız oldu"
};

/**
 * Buton grup change
 */
export const updateButtonGroupIndex = newIndex => {
  return dispatch => {
    dispatch({ type: types.ButtonGroup_CHANGED, payload: newIndex });
  };
};

/**
 * Email input change
 */
export const emailChanged = email => {
  const error = validate.single(email, { presence: true, email: true });

  return dispatch => {
    dispatch({ type: types.EMAIL_CHANGED, email, error });
  };
};

/**
 * password input change
 */
export const passwordChanged = password => {
  const error = validate.single(password, { length: { minimum: 6 } });

  return dispatch => {
    dispatch({ type: types.PASSWORD_CHANGED, password, error });
  };
};

/**
 * giriş ve kayıt olma işlemleri
 */
export const authUser = (isRegister, email, password, navigation) => {
  if (isRegister == 1)
    return dispatch => {
      regUser(dispatch, email, password, navigation);
    };
  else
    return dispatch => {
      loginUser(dispatch, email, password, navigation);
    };
};

const regUser = async (dispatch, email, password, navigation) => {
  dispatch({ type: types.REGISTER });

  try {
    await firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        dispatch({ type: types.REGISTER_SUCCESS });

        ToastAndroid.show(
          "Unutmayın sipariş verebilmeniz için Ayarlardan bilgilerinizi güncellemelisiniz..",
          ToastAndroid.LONG
        );

        /**
         * Doğrulama maili yolladı.
         */
        firebase.auth().currentUser.sendEmailVerification();

        navigation.navigate("S_Main");
      });
  } catch (error) {
    dispatch({ type: types.REGISTER_FAIL, payload: error.message });
  }
};

const loginUser = async (dispatch, email, password, navigation) => {
  dispatch({ type: types.LOGIN });

  try {
    await firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        dispatch({ type: types.LOGIN_SUCCESS });

        navigation.navigate("S_Main");
      });
  } catch (error) {
    dispatch({ type: types.LOGIN_FAIL, payload: error.message });
  }
};

import firebase from "firebase";
import _ from "lodash";
import { firebaselestirme } from "../configs/initFirebase";

export const types = {
  SELECTED_MENU_CHANCED: "Seçilen menü değiştirildi.",

  ALLMENU: "Tüm menuler getirilecek",
  ALLMENU_SUCCESS: "Tüm menuler alındı",
  ALLMENU_FAIL: "Tüm menuler çekilemedi"
};

export const getAllMenus = () => {
  return dispatch => {
    getAllMenus1(dispatch);
  };
};

export const setSelectedMenu = i => {
  return dispatch => {
    setSelectedMenu1(dispatch, i);
  };
};

const getAllMenus1 = async dispatch => {
  dispatch({ type: types.ALLMENU });

  try {
    await firebase
      .database()
      .ref("/firma1/subeler/sube1/menuler/")
      .on("value", snapshot => {
        const menuNames = firebaselestirme(snapshot.val());

        dispatch({ type: types.ALLMENU_SUCCESS, menuNames });
      });
  } catch (er) {
    dispatch({ type: types.ALLMENU_FAIL, payload: er });
  }
};

const setSelectedMenu1 = (dispatch, i) => {
  dispatch({ type: types.SELECTED_MENU_CHANCED, payload: i });
};

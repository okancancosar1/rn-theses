import firebase from "firebase";

export const types = {
  ISAUTHCHECK: "Herhangi bir giriş var mı kontrolü başladı.",
  ISAUTHCHECK_SUCCESS: "Giriş var",
  ISAUTHCHECK_SUCCESS2: "Giriş YOK",
  ISAUTHCHECK_FAIL: "Herhangi bir giriş var mı kontrolü başarısız oldu",

  ISMARKETOPENCHECK: "Dükkan açık mı kontrolü başladı.",
  ISMARKETOPENCHECK_SUCCESS1: "Dükkan açık",
  ISMARKETOPENCHECK_SUCCESS0: "Dükkan KAPALI",
  ISMARKETOPENCHECK_FAIL: "Dükkan açık mı kontrolü başarısız oldu"
};
/**
 * Herhangi bir giriş var mı?
 */
export const checkAnyAuth = navigate => {
  return dispatch => {
    checkAnyAuth1(dispatch, navigate);
  };
};
const checkAnyAuth1 = async (dispatch, navigate) => {
  dispatch({ type: types.ISAUTHCHECK });
  checkMarket(dispatch);
  try {
    await firebase.auth().onAuthStateChanged(user => {
      if (user) {
        dispatch({ type: types.ISAUTHCHECK_SUCCESS, payload: user });
        console.log("::user=>::", user);
        navigate("S_Main");
      } else {
        console.log("::onAuthStateChanged:: No user is signed in.");
        dispatch({ type: types.ISAUTHCHECK_SUCCESS2 });
      }
    });
  } catch (er) {
    dispatch({ type: types.ISAUTHCHECK_FAIL, payload: er });
  }
};

const checkMarket = async dispatch => {
  dispatch({ type: types.ISMARKETOPENCHECK });

  try {
    await firebase
      .database()
      .ref("/firma1/subeler/sube1/subeDurumu/durum")
      .on("value", snapshot => {
        if (snapshot.val() == 0) {
          dispatch({ type: types.ISMARKETOPENCHECK_SUCCESS0 });
        }
        if (snapshot.val() == 1) {
          dispatch({ type: types.ISMARKETOPENCHECK_SUCCESS1 });
        }
      });
  } catch (error) {
    dispatch({ type: types.ISMARKETOPENCHECK_FAIL, payload: error });
  }
};

import firebase from "firebase";
import { firebaselestirme } from "../configs/initFirebase";

export const types = {
  ORDER_DETAIL: "Sipariş detayları geliyor",
  ORDER_DETAIL_SUCCESS: "Sipariş detayları geldi",
  ORDER_DETAIL_FAIL: "Sipariş detayları gelemedi."
};

export const getOrderDetail = key => {
  return dispatch => {
    getOrderDetail1(dispatch, key);
  };
};

const getOrderDetail1 = async (dispatch, key) => {
  const userId = firebase.auth().currentUser.uid;
  dispatch({ type: types.ORDER_DETAIL });

  try {
    await firebase
      .database()
      .ref(`/firma1/uyeler/${userId}/oncekiSiparisler/${key}`)
      .on("value", snapshot => {
        if (snapshot.val()) {
          dispatch({
            type: types.ORDER_DETAIL_SUCCESS,
            payload: snapshot.val()
          });
        } else
          dispatch({
            type: types.ORDER_DETAIL_FAIL,
            payload: "Böyle bir sipariş bulunamadı."
          });
      });
  } catch (error) {
    dispatch({ type: types.ORDER_DETAIL_FAIL, payload: error });
  }
};

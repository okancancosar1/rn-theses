import firebase from "firebase";
import { firebaselestirme } from "../configs/initFirebase";

export const types = {
  GETTING_CART_COUNT_START: "Sepet büyüklüğü getiriliyor",
  GETTING_CART_COUNT_SUCCESS: "Sepet büyüklüğü geldi",
  GETTING_CART_COUNT_FAIL: "Sepet büyüklüğü gelirken gelemedi",

  IS_WORK: "sepet büyüklüğü daha önce çalıştı mı?"
};

export const getChartLenght = isWork => {
  return dispatch => {
    getChartLenght1(dispatch, isWork);
  };
};

const getChartLenght1 = (dispatch, isWork) => {
  if (isWork) return;

  dispatch({ type: types.GETTING_CART_COUNT_START });
  const userId = firebase.auth().currentUser.uid;

  try {
    firebase
      .database()
      .ref(`/firma1/uyeler/${userId}/sepet/`)
      .on("value", snapshot => {
        if (snapshot.val()) {
          const array = firebaselestirme(snapshot.val());
          dispatch({
            type: types.GETTING_CART_COUNT_SUCCESS,
            payload: array.length
          });
        } else dispatch({ type: types.GETTING_CART_COUNT_SUCCESS, payload: 0 });

        /**
         * Common bir component her defasında istek yapmaması için
         */
        dispatch({ type: types.IS_WORK, payload: true });
      });
  } catch (error) {
    dispatch({ type: types.GETTING_CART_COUNT_FAIL, payload: error });
  }
};

import firebase from "firebase";
import { firebaselestirme } from "../configs/initFirebase";

export const types = {
  GETTING_ORDERS: "Tüm siparişler geliyor",
  GETTING_ORDERS_SUCCESS: "Tüm siparişler geldi",
  GETTING_ORDERS_FAIL: "Tüm siparişler HATA"
};

export const getAllOrder = () => {
  return dispatch => {
    getAllOrder1(dispatch);
  };
};

const getAllOrder1 = async dispatch => {
  const userId = firebase.auth().currentUser.uid;
  dispatch({ type: types.GETTING_ORDERS });

  try {
    await firebase
      .database()
      .ref(`/firma1/uyeler/${userId}/oncekiSiparisler`)
      .limitToFirst(15)
      .on("value", snapshot => {
        if (snapshot.val()) {
          const array = firebaselestirme(snapshot.val());
          dispatch({ type: types.GETTING_ORDERS_SUCCESS, payload: array });
        } else {
          dispatch({
            type: types.GETTING_ORDERS_FAIL,
            payload: "Daha önce hiç sipariş vermemişsiniz."
          });
        }
      });
  } catch (error) {
    dispatch({ type: types.GETTING_ORDERS_FAIL, payload: error });
  }
};

import firebase from "firebase";
import { firebaselestirme } from "../configs/initFirebase";

export const types = {
  GETTING_ADDRESS: "Adresler alınmaya başladı.",
  GETTING_ADDRESS_SUCCESS: "Adresler alındi.",
  GETTING_ADDRESS_FAIL: "Adresler alınırken hata",

  GET_LAST_ORDER: "Son siparişin keyini alır"
};
/**
 * kisinin adreslerini getirir.
 */
export const getAddresses = () => {
  return dispatch => {
    getAddresses1(dispatch);
    getLastOrder(dispatch);
  };
};
const getAddresses1 = dispatch => {
  const userID = firebase.auth().currentUser.uid;
  dispatch({ type: types.GETTING_ADDRESS });
  try {
    firebase
      .database()
      .ref(`/firma1/uyeler/${userID}/adresler`)
      .on("value", snapshot => {
        let array = [];
        if (snapshot.val()) {
          array = firebaselestirme(snapshot.val());
          dispatch({
            type: types.GETTING_ADDRESS_SUCCESS,
            payload: array
          });
        } else
          dispatch({
            type: types.GETTING_ADDRESS_FAIL,
            payload: "Adres bulunamadı."
          });
      });
  } catch (error) {
    dispatch({ type: types.GETTING_ADDRESS_FAIL, payload: error });
  }
};

/**
 * kisinin adreslerini getirir.
 */
const getLastOrder = dispatch => {
  const userID = firebase.auth().currentUser.uid;

  return firebase
    .database()
    .ref(`/firma1/uyeler/${userID}/oncekiSiparisler`)
    .limitToFirst(1)
    .on("value", snapshot => {
      let array = [];
      if (snapshot.val()) {
        array = firebaselestirme(snapshot.val());
        dispatch({
          type: types.GET_LAST_ORDER,
          payload: array[0].key
        });
      }
    });
};

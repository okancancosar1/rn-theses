import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  View,
  Text,
  Image,
  ActivityIndicator
} from "react-native";
import { Provider } from "react-redux";

import CodePush from "react-native-code-push";
import * as Progress from "react-native-progress";

import store from "./configs/store";
import AppWithNavigationState from "./routers/AppWithNavigationState";
import firebaseInit from "./configs/initFirebase";

const CodePushConfig = {
  installMode: CodePush.InstallMode.IMMEDIATE,
  updateDialog: {
    title: "Ek paketler var",
    mandatoryUpdateMessage: "Ek paketler indirilecek...",
    mandatoryContinueButtonLabel: "Devam et"
  }
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progresss: 0.0,
      show: false,
      msg: "İndirme başlıyor..."
    };
  }

  _checkUpdateHandle = async () => {
    await CodePush.sync(
      CodePushConfig,
      status => {
        switch (status) {
          case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
            console.log("CUSTOM::Checking for updates.");
            break;
          case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
            console.log("CUSTOM::Downloading package.");
            this.setState({ show: true });
            break;
          case CodePush.SyncStatus.INSTALLING_UPDATE:
            console.log("CUSTOM::Installing update.");
            this.setState({ show: false });
            break;
          case CodePush.SyncStatus.UP_TO_DATE:
            console.log("CUSTOM::Up-to-date.");
            break;
          case CodePush.SyncStatus.UPDATE_INSTALLED:
            console.log("CUSTOM::Update installed.");
            break;
        }
      },
      ({ receivedBytes, totalBytes }) => {
        this.setState({ progresss: receivedBytes / totalBytes });

        const total = totalBytes * 0.0000009537;
        const received = receivedBytes * 0.0000009537;

        this.setState({
          msg: received.toFixed(2) + "mb/" + total.toFixed(2) + "mb tamamlandı."
        });
      }
    );
  };

  componentWillMount() {
    console.disableYellowBox = true;
    this._checkUpdateHandle();
    firebaseInit();
  }

  render() {
    if (this.state.show) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0f0" />
          <Text style={styles.text}> {this.state.msg} </Text>
          <Progress.Bar progress={this.state.progresss} width={200} />
        </View>
      );
    }
    return (
      <Provider store={store}>
        <AppWithNavigationState />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontFamily: "AmaticSC-Bold",
    fontSize: 23,
    marginBottom: 10
  }
});

App = CodePush(CodePushConfig)(App);
AppRegistry.registerComponent("BurgerHouse", () => App);

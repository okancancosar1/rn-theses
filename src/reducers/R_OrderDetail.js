import { types } from "../actions/A_OrderDetail";

const INITIAL_STATE = {
  selectedAddress: "",
  minPrice: 0,
  minTime: 0,

  orderNote: "",

  loading: false,
  error: ""
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    ////////////////////////
    case types.CHANGED_SELECTED_ADDRESS:
      return { ...state, selectedAddress: actions.payload };
    case types.CHANGED_MIN_PRICE:
      return { ...state, minPrice: actions.payload };
    case types.CHANGED_MIN_TIME:
      return { ...state, minTime: actions.payload };

    case types.ORDER_NOTE_CHANGED:
      return { ...state, orderNote: actions.payload };

    /////////////////////////////////
    case types.SET_ORDER:
      return { ...state, loading: true };
    case types.SET_ORDER_SUCCESS:
      return { ...state, loading: false, error: "" };
    case types.SET_ORDER_FAIL:
      return { ...state, error: actions.payload, loading: false };

    /////////////////
    case types.RESET:
      return INITIAL_STATE;

    default:
      return state;
  }
};

import { types } from "../actions/A_OrderSuccess";

const INITIAL_STATE = {
  data: {},
  loading: false,
  error: ""
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    case types.ORDER_DETAIL:
      return { ...state, loading: true };
    case types.ORDER_DETAIL_SUCCESS:
      return { ...state, data: actions.payload, loading: false, error: "" };
    case types.ORDER_DETAIL_FAIL:
      return { ...state, error: actions.payload, loading: false };

    default:
      return state;
  }
};

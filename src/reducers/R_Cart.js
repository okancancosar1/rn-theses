import { types } from "../actions/A_Cart";

const INITIAL_STATE = {
  cartItems: [],
  loading: false,
  error: "",

  totalPrice: 0
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    //////////////////////////////
    case types.GETTING_CART_START:
      return { ...state, loading: true };
    case types.GETTING_CART_SUCCESS:
      return {
        ...state,
        cartItems: actions.payload,
        loading: false,
        error: ""
      };
    case types.GETTING_CART_FAIL:
      return {
        ...state,
        error: actions.payload,
        loading: false,
        cartItems: [],
        totalPrice: 0
      };

    /////////////////////////////
    case types.UPDATE_TOTAL:
      return { ...state, totalPrice: actions.payload };

    default:
      return state;
  }
};

import { types } from "../actions/A_ProductDetail";

const INITIAL_STATE = {
  productDetail: "",
  detailLoading: false,
  detailError: "",

  productCount: 1,

  extraSelectedItems: [],
  drinkSelectedItems: [],

  productPrice: 0,
  drinkPrice: 0,
  extrasPrice: 0,
  totalPrice: 0,

  loading: false,
  error: "",

  isFav: ""
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    ////////////////
    case types.GET_PRODUCT_DETAIL:
      return { ...state, detailLoading: true };
    case types.GET_PRODUCT_DETAIL_SUCCESS:
      return { ...state, productDetail: actions.payload, detailLoading: false };
    case types.GET_PRODUCT_DETAIL_FAIL:
      return { ...state, detailError: actions.payload, detailLoading: false };

    //////////////////////
    case types.PRODUCT_COUNT_INCREASE:
      return { ...state, productCount: Number(actions.payload) + 1 };
    case types.PRODUCT_COUNT_DECREASE:
      return { ...state, productCount: Number(actions.payload) - 1 };

    ////////////////
    case types.CHANGE_EXTRA_SELECTED:
      return { ...state, extraSelectedItems: actions.payload };
    case types.CHANGE_DRINK_SELECTED:
      return { ...state, drinkSelectedItems: actions.payload };

    ////////////////
    case types.CHANGE_PRODUCT_PRICE:
      return { ...state, productPrice: actions.payload };
    case types.CHANGE_DRINK_PRICE:
      return { ...state, drinkPrice: actions.payload };
    case types.CHANGE_EXTRAS_PRICE:
      return { ...state, extrasPrice: actions.payload };
    case types.CHANGE_TOTAL_PRICE:
      return { ...state, totalPrice: actions.payload };

    ////////////////
    case types.ADD_TO_CART:
      return { ...state, loading: true };
    case types.ADD_TO_CART_SUCCESS:
      return { ...state, loading: false };
    case types.ADD_TO_CART_FAIL:
      return { ...state, error: actions.payload, loading: false };

    //////////////
    case types.RESET_REDUCERS:
      return INITIAL_STATE;

    case types.CHECK_IS_FAV:
      return { ...state, isFav: actions.payload };

    default:
      return state;
  }
};

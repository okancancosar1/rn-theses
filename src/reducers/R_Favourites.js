import { types } from "../actions/A_Favourites";

const INITIAL_STATE = {
  allFavs: [],
  loading: false,
  error: ""
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    ///////////////////////
    case types.ALL_FAV:
      return { ...state, loading: true };
    case types.ALL_FAV_SUCCESS:
      return { ...state, allFavs: actions.payload, loading: false, error: "" };
    case types.ALL_FAV_FAIL:
      return { ...state, error: actions.payload, loading: false };

    default:
      return state;
  }
};

import { types } from "../actions/A_NewAdress";

const INITIAL_STATE = {
  type: "",
  title: "",
  phone: "",
  semt: "",
  address: "",
  directions: "",

  tumSemtler: [],

  loading: false,
  error: ""
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    ///////////////////////////
    case types.TYPE_CHANGED:
      return { ...state, type: actions.payload };
    case types.TITLE_CHANGED:
      return { ...state, title: actions.payload };
    case types.PHONE_CHANGED:
      return { ...state, phone: actions.payload };
    case types.SEMT_CHANGED:
      return { ...state, semt: actions.payload };
    case types.ADRESS_CHANGED:
      return { ...state, address: actions.payload };
    case types.DIRECTIONS_CHANGED:
      return { ...state, directions: actions.payload };
    ///////////////////////////

    case types.GETSEMT:
      return { ...state, tumSemtler: actions.payload };

    case types.ADD_ADDRESS:
      return { ...state, loading: true };
    case types.ADD_ADDRESS_SUCCESS:
      return INITIAL_STATE;
    case types.ADD_ADDRESS_FAIL:
      return { ...state, error: actions.payload, loading: false };

    ///////////////
    case types.RESET:
      return INITIAL_STATE;

    default:
      return state;
  }
};

import { types } from "../actions/A_Auth";

const INITIAL_STATE = {
  selectedButtonGroupIndex: 0,
  email: "",
  emailError: "",
  password: "",
  passwordError: "",

  loading: false,
  error: ""
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    //////////////////////
    case types.REGISTER:
      return { ...state, loading: true };
    case types.REGISTER_SUCCESS:
      return { ...state, loading: false };
    case types.REGISTER_FAIL:
      return { ...state, loading: false, error: actions.payload };

    //////////////////////
    case types.LOGIN:
      return { ...state, loading: true };
    case types.LOGIN_SUCCESS:
      return INITIAL_STATE;
    case types.LOGIN_FAIL:
      return { ...state, loading: false, error: actions.payload };

    //////////////////////
    case types.ButtonGroup_CHANGED:
      return {
        ...state,
        selectedButtonGroupIndex: actions.payload,
        passwordError: "",
        emailError: ""
      };

    //////////////////////
    case types.EMAIL_CHANGED:
      return { ...state, email: actions.email, emailError: actions.error };
    case types.PASSWORD_CHANGED:
      return {
        ...state,
        password: actions.password,
        passwordError: actions.error
      };

    default:
      return state;
  }
};

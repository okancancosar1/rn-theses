import { types } from "../actions/A_Orders";

const INITIAL_STATE = {
  siparisler: [],
  loading: false,
  error: ""
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    ////////////////////////////
    case types.GETTING_ORDERS:
      return { ...state, loading: true };
    case types.GETTING_ORDERS_SUCCESS:
      return {
        ...state,
        siparisler: actions.payload,
        loading: false,
        error: ""
      };
    case types.GETTING_ORDERS_FAIL:
      return { ...state, error: actions.payload, loading: false };

    default:
      return state;
  }
};

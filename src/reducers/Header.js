import { types } from "../actions/Header";

const INITIAL_STATE = {
  sepetBuyuklugu: 0,
  loading: false,
  error: "",

  isWork: false
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    case types.GETTING_CART_COUNT_START:
      return { ...state, loading: true };
    case types.GETTING_CART_COUNT_SUCCESS:
      return {
        ...state,
        sepetBuyuklugu: actions.payload,
        loading: false,
        error: ""
      };
    case types.GETTING_CART_COUNT_FAIL:
      return { ...state, error: actions.payload, loading: false };

    ///////////////////////
    case types.IS_WORK:
      return { ...state, isWork: actions.payload };

    default:
      return state;
  }
};

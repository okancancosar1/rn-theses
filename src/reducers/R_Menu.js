import { types } from "../actions/A_Menu";

const INITIAL_STATE = {
  menuNames: [],

  product: {},
  loading: false,
  error: "",

  selectedMenu: 0
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    /////////////////////
    case types.ALLMENU:
      return { ...state, loading: true };
    case types.ALLMENU_SUCCESS:
      return {
        ...state,
        menuNames: actions.menuNames,
        loading: false,
        error: ""
      };
    case types.ALLMENU_FAIL:
      return { ...state, error: actions.payload, loading: false };

    ///////////////
    case types.SELECTED_MENU_CHANCED:
      return { ...state, selectedMenu: actions.payload };

    default:
      return state;
  }
};

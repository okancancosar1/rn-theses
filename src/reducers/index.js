import { combineReducers } from "redux";

import nav from "./navReducers";
import Header from "./Header";
import R_Welcome from "./R_Welcome";
import R_Auth from "./R_Auth";
import R_Main from "./R_Main";
import R_UpdateProfile from "./R_UpdateProfile";
import R_Menu from "./R_Menu";
import R_ProductDetail from "./R_ProductDetail";
import R_Settings from "./R_Settings";
import R_NewAdress from "./R_NewAdress";
import R_Favourites from "./R_Favourites";
import R_Cart from "./R_Cart";
import R_OrderDetail from "./R_OrderDetail";
import R_OrderSuccess from "./R_OrderSuccess";
import R_Orders from "./R_Orders";

const appReducer = combineReducers({
  nav,
  Header,
  R_Welcome,
  R_Auth,
  R_Main,
  R_UpdateProfile,
  R_Menu,
  R_ProductDetail,
  R_Settings,
  R_NewAdress,
  R_Favourites,
  R_Cart,
  R_OrderDetail,
  R_OrderSuccess,
  R_Orders
});

const rootReducer = (state, action) => {
  /**
   * Kullanıcı çıkış yaptığında tüm state'leri silmemiz gerekiyor.
   */
  if (action.type === "USER_LOGOUT") {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;

import { types } from "../actions/A_Main";

const INITIAL_STATE = {
  adresler: [],
  loading: false,
  error: "",

  lastOrder: ""
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    case types.GETTING_ADDRESS:
      return { ...state, loading: true };
    case types.GETTING_ADDRESS_SUCCESS:
      return { ...state, adresler: actions.payload, loading: false, error: "" };
    case types.GETTING_ADDRESS_FAIL:
      return { ...state, error: actions.payload, loading: false };

    ///////////////////////////
    case types.GET_LAST_ORDER:
      return { ...state, lastOrder: actions.payload };

    default:
      return state;
  }
};

import { types } from "../actions/A_UpdateProfile";

const INITIAL_STATE = {
  name: "",
  surname: "",
  phone: "",

  loading: false,
  error: ""
};
export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    //////////////////////
    case types.UPDATE:
      return { ...state, loading: true };
    case types.UPDATE_SUCCESS:
      return INITIAL_STATE;
    case types.UPDATE_FAIL:
      return { ...state, loading: false, error: actions.payload };

    //////////////////////
    case types.NAME_CHANGED:
      return { ...state, name: actions.payload };
    case types.SURNAME_CHANGED:
      return { ...state, surname: actions.payload };
    case types.PHONE_CHANGED:
      return { ...state, phone: actions.payload };

    default:
      return state;
  }
};

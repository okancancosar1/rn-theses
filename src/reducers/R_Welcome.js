import { types } from "../actions/A_Welcome";

const INITIAL_STATE = {
  isAuth: false,
  showImage: false,
  authError: "",
  isOpen: false,
  isOpenError: "",
  user: ""
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    //////////////////////
    case types.ISAUTHCHECK:
      return { ...state, showImage: true };
    case types.ISAUTHCHECK_SUCCESS:
      return {
        ...state,
        isAuth: true,
        showImage: false,
        user: actions.payload
      };
    case types.ISAUTHCHECK_SUCCESS2:
      return { ...state, isAuth: false, showImage: false };
    case types.ISAUTHCHECK_FAIL:
      return { ...state, authError: actions.payload, showImage: false };

    //////////////////////
    case types.ISMARKETOPENCHECK:
      return { ...state };
    case types.ISMARKETOPENCHECK_SUCCESS0:
      return { ...state, isOpen: false };
    case types.ISMARKETOPENCHECK_SUCCESS1:
      return { ...state, isOpen: true };
    case types.ISMARKETOPENCHECK_FAIL:
      return { ...state, isOpenError: actions.payload };

    default:
      return state;
  }
};

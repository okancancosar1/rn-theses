import { types } from "../actions/A_Settings";

const INITIAL_STATE = {
  isAuth: false,
  authError: "",
  user: ""
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    //////////////////////
    case types.ISAUTHCHECK:
      return { ...state };
    case types.ISAUTHCHECK_SUCCESS:
      return {
        ...state,
        isAuth: true,
        user: actions.payload,
        authError: ""
      };
    case types.ISAUTHCHECK_SUCCESS2:
      return { ...state, isAuth: false };
    case types.ISAUTHCHECK_FAIL:
      return { ...state, authError: actions.payload };

    default:
      return state;
  }
};

import React, { Component } from "react";
import { BackHandler } from "react-native";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { addNavigationHelpers, NavigationActions } from "react-navigation";
import { createReduxBoundAddListener } from "react-navigation-redux-helpers";

import App from "./App";
import { addListener } from "../configs/redux";

class AppWithNavigationState extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }

  onBackPress = () => {
    const { dispatch, nav } = this.props;

    const stack = nav.routes[0].routes[0];
    const stackIndex = stack.index;

    console.log("::stack::", stack);
    console.log("::stackIndex::", stackIndex);

    if (stack.index === 0) {
      return false;
    } else if (
      //Sipariş verildikten sonra
      stack.routes[stackIndex].routeName == "S_OrderSuccess" &&
      stack.routes[stackIndex - 1].routeName == "S_OrderDetail"
    ) {
      console.log("::SETTING AN ORDER::");
      dispatch(
        NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "S_Main" })]
        })
      );
    } else if (
      //login ve sign in yapıldığında
      stack.routes[stackIndex].routeName == "S_Main" &&
      stack.routes[stackIndex - 1].routeName == "S_Auth"
    ) {
      console.log("::LOGIN OR SIGNIN::");
      dispatch(
        NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "S_Main" })]
        })
      );
    } else if (
      //login ve sign in yapıldığında
      stack.routes[stackIndex].routeName == "S_Adresses" &&
      stack.routes[stackIndex - 1].routeName == "S_NewAdress"
    ) {
      console.log("::ADDING NEW ADDRESS::");
      dispatch(
        NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "S_Main" })]
        })
      );
    } else if (
      // Uygulama açıldı welcome ekranı tekrar gelmesin.
      stack.routes[stackIndex].routeName == "S_Main" &&
      stack.routes[stackIndex - 1].routeName == "S_Welcome"
    ) {
      console.log("::AUTO LOGİN::");
      return false;
    }

    dispatch(NavigationActions.back());
    return true;
  };

  render() {
    const { dispatch, nav } = this.props;
    return (
      <App
        navigation={addNavigationHelpers({
          dispatch,
          state: nav,
          addListener
        })}
      />
    );
  }
}

export default connect(state => ({ nav: state.nav }))(AppWithNavigationState);

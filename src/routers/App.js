import React from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import CardStackStyleInterpolator from "react-navigation/src/views/CardStack/CardStackStyleInterpolator";

import DrawerContainer from "../configs/DrawerContainer";

import S_Welcome from "../screens/S_Welcome";
import S_Auth from "../screens/S_Auth";
import S_UpdateProfile from "../screens/S_UpdateProfile";
import S_Main from "../screens/S_Main";
import S_Menu from "../screens/S_Menu";
import S_ProductDetail from "../screens/S_ProductDetail";
import S_Settings from "../screens/S_Settings";
import S_Adresses from "../screens/S_Adresses";
import S_NewAdress from "../screens/S_NewAdress";
import S_Favourites from "../screens/S_Favourites";
import S_Cart from "../screens/S_Cart";
import S_OrderDetail from "../screens/S_OrderDetail";
import S_OrderSuccess from "../screens/S_OrderSuccess";
import S_Orders from "../screens/S_Orders";
import S_ResetPass from "../screens/S_ResetPass";

const App = StackNavigator(
  {
    S_Welcome: {
      screen: S_Welcome,
      navigationOptions: {
        header: null
      }
    },
    S_Auth: {
      screen: S_Auth,
      navigationOptions: {
        header: null
      }
    },
    S_UpdateProfile: {
      screen: S_UpdateProfile,
      navigationOptions: {
        header: null
      }
    },
    S_Main: {
      screen: S_Main,
      navigationOptions: {
        header: null
      }
    },
    S_Menu: {
      screen: S_Menu,
      navigationOptions: {
        header: null
      }
    },
    S_ProductDetail: {
      screen: S_ProductDetail,
      navigationOptions: {
        header: null
      }
    },
    S_Settings: {
      screen: S_Settings,
      navigationOptions: {
        header: null
      }
    },
    S_Adresses: {
      screen: S_Adresses,
      navigationOptions: {
        header: null
      }
    },
    S_NewAdress: {
      screen: S_NewAdress,
      navigationOptions: {
        header: null
      }
    },
    S_Favourites: {
      screen: S_Favourites,
      navigationOptions: {
        header: null
      }
    },
    S_Cart: {
      screen: S_Cart,
      navigationOptions: {
        header: null
      }
    },
    S_OrderDetail: {
      screen: S_OrderDetail,
      navigationOptions: {
        header: null
      }
    },
    S_OrderSuccess: {
      screen: S_OrderSuccess,
      navigationOptions: {
        header: null
      }
    },
    S_Orders: {
      screen: S_Orders,
      navigationOptions: {
        header: null
      }
    },
    S_ResetPass: {
      screen: S_ResetPass,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    headerMode: "screen",
    transitionConfig: () => ({
      screenInterpolator: CardStackStyleInterpolator.forHorizontal,
      transitionSpec: {
        duration: 250
      }
    })
  }
);

const deneme = DrawerNavigator(
  {
    ALL: { screen: App }
  },
  {
    gesturesEnabled: false,
    contentComponent: DrawerContainer
  }
);
export default deneme;

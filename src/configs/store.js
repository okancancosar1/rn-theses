import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";

import rootReducer from "../reducers";
import { navMiddleware } from "./redux";

const middlewares = [thunk, navMiddleware];

if (__DEV__) {
  middlewares.push(logger);
}

export default createStore(rootReducer, applyMiddleware(...middlewares));

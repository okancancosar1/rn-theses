import * as firebase from "firebase";

const firebaseInit = async dispatch => {
  const config = {
    apiKey: "AIzaSyALXFj-EhTIlhj8ieOD8mAv8fZUelOOn-M",
    authDomain: "ressis-restaurant-sistem.firebaseapp.com",
    databaseURL: "https://ressis-restaurant-sistem.firebaseio.com",
    projectId: "ressis-restaurant-sistem",
    storageBucket: "ressis-restaurant-sistem.appspot.com",
    messagingSenderId: "228374157630"
  };
  await firebase.initializeApp(config);
};

export default firebaseInit;

/**
 * Firebaseden gelen dataların flatliste uygun hale getiriyor.
 * @param {*} data
 */
export const firebaselestirme = data => {
  return Object.entries(data).map(e => Object.assign(e[1], { key: e[0] }));
};

import React, { Component } from "react";
import {
  ScrollView,
  StyleSheet,
  ImageBackground,
  Text,
  View,
  Image,
  ToastAndroid
} from "react-native";
import { SafeAreaView } from "react-navigation";
import { ListItem } from "react-native-elements";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { signout } from "../actions/A_Settings";
import { Information } from "../../Ortak";
import { Separator } from "../screens/Common";

class DrawerContainer extends Component {
  render() {
    const { navigate } = this.props.navigation;
    const user = this.props.user ? this.props.user : "";
    const displayName =
      this.props.user.displayName || this.props.user.displayName != ""
        ? this.props.user.displayName
        : "";

    /**
     * Şuanki olduğu routenamei getiriyo
     */
    const stack = this.props.nav.routes[0].routes[0];
    const stackIndex = stack.index;
    const location = stack.routes[stackIndex].routeName;

    return (
      <ScrollView>
        <SafeAreaView
          style={styles.container}
          forceInset={{ top: "always", horizontal: "never" }}
        >
          <ImageBackground
            source={Information.drawerBackground}
            style={styles.logoCont}
          >
            <Image
              resizeMode="stretch"
              style={styles.logo}
              source={Information.logo}
            />
          </ImageBackground>

          <View style={styles.cont}>
            {/* Sık kullanılanlar */}
            <View style={styles.racentCont}>
              <Text> Hoşgeldiniz {displayName} </Text>
              <ListItem
                key={Math.random().toString()}
                title={"Menüler"}
                leftIcon={{ name: "food", type: "material-community" }}
                onPress={() => {
                  if (location != "S_Menu") navigate("S_Menu");
                  else navigate("DrawerClose");
                }}
              />

              <ListItem
                key={Math.random().toString()}
                title={"Son Sipariş Detayı"}
                leftIcon={{ name: "details", type: "material" }}
                onPress={() => {
                  if (this.props.lastOrder) {
                    if (location != "S_OrderSuccess")
                      navigate("S_OrderSuccess", {
                        newKey: this.props.lastOrder
                      });
                    else navigate("DrawerClose");
                  } else {
                    if (this.props.isAuth) {
                      ToastAndroid.show(
                        "Daha önce bir sipariş vermemişsiniz.",
                        ToastAndroid.SHORT
                      );
                    } else {
                      ToastAndroid.show(
                        "Son sipariş detayınızı burada göreceksiniz. ",
                        ToastAndroid.SHORT
                      );
                    }
                  }
                }}
              />
              <ListItem
                key={Math.random().toString()}
                title={"Önceki Siparişler"}
                leftIcon={{ name: "history", type: "material-community" }}
                onPress={() => {
                  if (location != "S_Orders") navigate("S_Orders");
                  else navigate("DrawerClose");
                }}
              />
              <ListItem
                key={Math.random().toString()}
                title={"Favoriler"}
                leftIcon={{ name: "favorite-border", type: "material" }}
                onPress={() => {
                  if (location != "S_Favourites") navigate("S_Favourites");
                  else navigate("DrawerClose");
                }}
              />
              <ListItem
                key={Math.random().toString()}
                title={"Ayarlar"}
                leftIcon={{ name: "settings", type: "simple-line-icon" }}
                onPress={() => {
                  if (location != "S_Settings") navigate("S_Settings");
                  else navigate("DrawerClose");
                }}
              />
              {this.props.isAuth ? (
                <ListItem
                  key={Math.random().toString()}
                  title={"Çıkış yap"}
                  leftIcon={{ name: "exit-to-app" }}
                  onPress={() =>
                    this.props.actions.signout(this.props.navigation)
                  }
                />
              ) : (
                <ListItem
                  key={Math.random().toString()}
                  title={"Giriş yap"}
                  leftIcon={{ name: "all-inclusive" }}
                  onPress={() => {
                    if (location != "S_Auth") navigate("S_Auth");
                    else navigate("DrawerClose");
                  }}
                />
              )}
            </View>

            {/* Marka */}
            <View style={styles.bottomCont}>
              <Text style={styles.corp}> Ressis Restoran Hizmetleri </Text>
              <Text style={styles.copyright}> Copyright © 2018 </Text>
            </View>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  logoCont: {
    justifyContent: "center",
    alignItems: "center",
    padding: 30
  },
  logo: {
    width: 100,
    height: 100
  },
  cont: {
    flex: 1
  },
  racentCont: {
    flex: 1,
    marginVertical: 10
  },
  bottomCont: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  corp: {
    fontFamily: "AmaticSC-Bold",
    fontSize: 25
  },
  copyright: {
    fontFamily: "AmaticSC-Regular",
    fontSize: 20
  }
});

export default connect(
  state => ({
    nav: state.nav,
    isAuth: state.R_Welcome.isAuth,
    adresler: state.R_Main.adresler,
    error: state.R_Main.error,
    loading: state.R_Main.loading,
    lastOrder: state.R_Main.lastOrder,
    user: state.R_Welcome.user
  }),
  dispatch => ({
    actions: bindActionCreators(Object.assign({ signout }), dispatch)
  })
)(DrawerContainer);
